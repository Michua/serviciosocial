$(document).ready(function(){
  $("#validar").click(function(){
    $("#form_validation").validate({
      rules:{
        titulo:{
          required:true,
          maxlength:5,
        },
        autor:{
          required:true,
        },
        clasificacion:{
          required:true,
        },
       

      },
         messages:{
        titulo:{
          required:"Título requerido",
           maxlength:"Ingrese 13 caracteres"
        },
        autor:{
          required:"Autor requerido",
        },
        clasificacion:{
          required:"Clasificación requerida",
        },
       
      },
    });
  });

});