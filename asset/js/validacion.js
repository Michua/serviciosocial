$(document).ready(function () {
    $("#enviar").click(function () {
        $("#validar").validate({
            rules:{
                nombre:{
                    required: true,
                    minlength: 3
                },
                telefono:{
                    required: true,
                    digits: true,
                },
                pass:{
                    required: true,
                },
                pass_again:{
                    equalTo: "#pass"
                }
            },
            messages:{
                nombre:{
                    required: "El campo nombre es requerido",
                    minlength: "El campo requiere de {0} caracteres minimo"
                },
                telefono:{
                    required: "El campo telefono es requerido",
                    digits: "Solo digitos",
                },
                pass:{
                    required: "La contraseña es requerida"
                },
                pass_again:{
                    equalTo: "La contraseña no coincide"
                }
            }
        });

    });
});