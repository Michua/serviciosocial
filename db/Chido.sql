-- phpMyAdmin SQL Dump
-- version 4.8.2
-- https://www.phpmyadmin.net/
--
-- Servidor: 127.0.0.1
-- Tiempo de generación: 02-07-2019 a las 21:08:21
-- Versión del servidor: 10.1.34-MariaDB
-- Versión de PHP: 5.6.37

SET SQL_MODE = "NO_AUTO_VALUE_ON_ZERO";
SET AUTOCOMMIT = 0;
START TRANSACTION;
SET time_zone = "+00:00";


/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8mb4 */;

--
-- Base de datos: `prueba`
--

-- --------------------------------------------------------

--
-- Estructura de tabla para la tabla `blueray`
--

CREATE TABLE `blueray` (
  `id_clave` int(11) NOT NULL,
  `id_tipo` int(11) NOT NULL,
  `titulo` int(100) NOT NULL,
  `pais` varchar(100) DEFAULT NULL,
  `anio` int(11) DEFAULT NULL,
  `director` varchar(100) DEFAULT NULL,
  `reparto` varchar(100) DEFAULT NULL,
  `calificacion` float(3,1) DEFAULT NULL,
  `link` varchar(150) DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Volcado de datos para la tabla `blueray`
--

INSERT INTO `blueray` (`id_clave`, `id_tipo`, `pais`, `anio`, `director`, `reparto`, `calificacion`, `link`) VALUES
(4, 4, 'Mexico', 1998, 'Brandon Rios', '12A', 9.8, 'www.aaa.com');

-- --------------------------------------------------------

--
-- Estructura de tabla para la tabla `libros`
--

CREATE TABLE `libros` (
  `id_clave` int(11) NOT NULL,
  `id_tipo` int(11) NOT NULL,
  `autor` varchar(100) DEFAULT NULL,
  `pie_imprenta` varchar(150) DEFAULT NULL,
  `paginas` int(11) DEFAULT NULL,
  `isbn` varchar(100) DEFAULT NULL,
  `no_adquisicion` varchar(100) DEFAULT NULL,
  `temas` varchar(200) DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Volcado de datos para la tabla `libros`
--

INSERT INTO `libros` (`id_clave`, `id_tipo`, `autor`, `pie_imprenta`, `paginas`, `isbn`, `no_adquisicion`, `temas`) VALUES
(2, 2, 'Monica', 'ASD12', 12, 'ASFG', '14A', 'Novela');

-- --------------------------------------------------------

--
-- Estructura de tabla para la tabla `material`
--

CREATE TABLE `material` (
  `id_clave` int(11) NOT NULL,
  `id_tipo` int(11) NOT NULL,
  `titulo` varchar(100) NOT NULL,
  `clasificacion` varchar(50) DEFAULT NULL,
  `disponible` tinyint(1) DEFAULT '1'
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Volcado de datos para la tabla `material`
--

INSERT INTO `material` (`id_clave`, `id_tipo`, `titulo`, `clasificacion`, `disponible`) VALUES
(1, 1, 'MusicaUno', 'amor', 0),
(2, 2, 'LibroUno', 'Romantico', 1),
(3, 3, 'PeliculaUno', 'Terror', 0),
(4, 4, 'BluerayUno', 'Comedia', 1),
(5, 1, 'MusicaDos', 'amor', 0),
(6, 1, 'MusicaTres', 'Romantico', 1),
(7, 1, 'Musica4', 'amor', 1),
(8, 1, 'musica5', 'amor', 1),
(9, 1, 'musica6', 'amor', 1),
(10, 1, 'musica7', 'amor', 1),
(11, 1, 'musica8', 'amor', 1),
(12, 1, 'musica9', 'amor', 1),
(13, 1, 'musica10', 'amor', 1),
(14, 1, 'musica11', 'amor', 1),
(15, 1, 'musica12', 'amor', 1),
(16, 1, 'musica13', 'amor', 1),
(17, 1, 'musica13', 'amor', 1),
(18, 1, 'musica14', 'amor', 1),
(19, 1, 'musica15', 'amor', 1),
(20, 1, 'musica16', 'amor', 1),
(21, 1, 'musica17', 'amor', 1);

-- --------------------------------------------------------

--
-- Estructura de tabla para la tabla `musica`
--

CREATE TABLE `musica` (
  `id_clave` int(11) NOT NULL,
  `id_tipo` int(11) NOT NULL,
  `interprete` varchar(100) DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Volcado de datos para la tabla `musica`
--

INSERT INTO `musica` (`id_clave`, `id_tipo`, `interprete`) VALUES
(1, 1, 'Lore'),
(5, 1, 'Aracely'),
(6, 1, 'Jaqueline Neveros '),
(7, 1, 'Hola'),
(8, 1, 'h');

-- --------------------------------------------------------

--
-- Estructura de tabla para la tabla `peliculas`
--

CREATE TABLE `peliculas` (
  `id_clave` int(11) NOT NULL,
  `id_tipo` int(11) NOT NULL,
  `pais` varchar(100) DEFAULT NULL,
  `anio` int(11) DEFAULT NULL,
  `director` varchar(100) DEFAULT NULL,
  `reparto` varchar(100) DEFAULT NULL,
  `calificacion` float(3,1) DEFAULT NULL,
  `region` varchar(150) DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Volcado de datos para la tabla `peliculas`
--

INSERT INTO `peliculas` (`id_clave`, `id_tipo`, `pais`, `anio`, `director`, `reparto`, `calificacion`, `region`) VALUES
(3, 3, 'Mexico', 1785, 'Pablo', 'ASFS', 5.8, 'Centro');

-- --------------------------------------------------------

--
-- Estructura de tabla para la tabla `prestadores`
--

CREATE TABLE `prestadores` (
  `id_prestador` int(11) NOT NULL,
  `nombre` varchar(100) DEFAULT NULL,
  `ape_pat` varchar(100) DEFAULT NULL,
  `ape_mat` varchar(100) DEFAULT NULL,
  `correo` varchar(150) DEFAULT NULL,
  `telefono` varchar(10) DEFAULT NULL,
  `contrasena` varchar(100) DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Volcado de datos para la tabla `prestadores`
--

INSERT INTO `prestadores` (`id_prestador`, `nombre`, `ape_pat`, `ape_mat`, `correo`, `telefono`, `contrasena`) VALUES
(1, 'Jesus', 'Gomez', 'Diaz', 'jesus@hotmail.com', '7224567832', '123456'),
(2, 'Monica', 'Salinas', 'C', 'asda', '7772134590', '123456');

-- --------------------------------------------------------

--
-- Estructura de tabla para la tabla `prestamos`
--

CREATE TABLE `prestamos` (
  `id_prestamo` int(11) NOT NULL,
  `id_clave` int(11) NOT NULL,
  `id_prestador` int(11) DEFAULT NULL,
  `id_usuario` int(11) DEFAULT NULL,
  `fecha_prestamo` date DEFAULT NULL,
  `fecha_prometida` date DEFAULT NULL,
  `fecha_devolucion` date DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Volcado de datos para la tabla `prestamos`
--

INSERT INTO `prestamos` (`id_prestamo`, `id_clave`, `id_prestador`, `id_usuario`, `fecha_prestamo`, `fecha_prometida`, `fecha_devolucion`) VALUES
(30, 1, 1, 1, '2019-07-02', '2019-07-05', NULL),
(31, 3, 1, 1, '2019-07-02', '2019-07-05', NULL),
(32, 4, 1, 2, '2019-07-02', '2019-07-05', '2019-07-02'),
(33, 5, 1, 2, '2019-07-02', '2019-07-05', NULL),
(34, 6, 1, 2, '2019-07-02', '2019-07-05', '2019-07-02');

-- --------------------------------------------------------

--
-- Estructura de tabla para la tabla `usuarios`
--

CREATE TABLE `usuarios` (
  `id_usuario` int(11) NOT NULL,
  `nombre` varchar(100) DEFAULT NULL,
  `ape_pat` varchar(100) DEFAULT NULL,
  `ape_mat` varchar(100) DEFAULT NULL,
  `correo` varchar(100) DEFAULT NULL,
  `telefono` varchar(10) DEFAULT NULL,
  `adeudo` int(11) DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Volcado de datos para la tabla `usuarios`
--

INSERT INTO `usuarios` (`id_usuario`, `nombre`, `ape_pat`, `ape_mat`, `correo`, `telefono`, `adeudo`) VALUES
(2, 'Aracely', 'Gomez', 'Di', 'a', '7621002062', 0);

--
-- Índices para tablas volcadas
--

--
-- Indices de la tabla `blueray`
--
ALTER TABLE `blueray`
  ADD PRIMARY KEY (`id_clave`,`id_tipo`);

--
-- Indices de la tabla `libros`
--
ALTER TABLE `libros`
  ADD PRIMARY KEY (`id_clave`,`id_tipo`);

--
-- Indices de la tabla `material`
--
ALTER TABLE `material`
  ADD PRIMARY KEY (`id_clave`,`id_tipo`);

--
-- Indices de la tabla `musica`
--
ALTER TABLE `musica`
  ADD PRIMARY KEY (`id_clave`,`id_tipo`);

--
-- Indices de la tabla `peliculas`
--
ALTER TABLE `peliculas`
  ADD PRIMARY KEY (`id_clave`,`id_tipo`);

--
-- Indices de la tabla `prestadores`
--
ALTER TABLE `prestadores`
  ADD PRIMARY KEY (`id_prestador`);

--
-- Indices de la tabla `prestamos`
--
ALTER TABLE `prestamos`
  ADD PRIMARY KEY (`id_prestamo`,`id_clave`),
  ADD UNIQUE KEY `id_clave` (`id_clave`) USING BTREE,
  ADD KEY `id_prestador` (`id_prestador`),
  ADD KEY `id_usuario` (`id_usuario`);

--
-- Indices de la tabla `usuarios`
--
ALTER TABLE `usuarios`
  ADD PRIMARY KEY (`id_usuario`);

--
-- AUTO_INCREMENT de las tablas volcadas
--

--
-- AUTO_INCREMENT de la tabla `material`
--
ALTER TABLE `material`
  MODIFY `id_clave` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=22;

--
-- AUTO_INCREMENT de la tabla `prestamos`
--
ALTER TABLE `prestamos`
  MODIFY `id_prestamo` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=35;

--
-- Restricciones para tablas volcadas
--

--
-- Filtros para la tabla `blueray`
--
ALTER TABLE `blueray`
  ADD CONSTRAINT `blueray_ibfk_1` FOREIGN KEY (`id_clave`,`id_tipo`) REFERENCES `material` (`id_clave`, `id_tipo`) ON DELETE CASCADE;

--
-- Filtros para la tabla `libros`
--
ALTER TABLE `libros`
  ADD CONSTRAINT `libros_ibfk_1` FOREIGN KEY (`id_clave`,`id_tipo`) REFERENCES `material` (`id_clave`, `id_tipo`) ON DELETE CASCADE;

--
-- Filtros para la tabla `musica`
--
ALTER TABLE `musica`
  ADD CONSTRAINT `musica_ibfk_1` FOREIGN KEY (`id_clave`,`id_tipo`) REFERENCES `material` (`id_clave`, `id_tipo`) ON DELETE CASCADE;

--
-- Filtros para la tabla `peliculas`
--
ALTER TABLE `peliculas`
  ADD CONSTRAINT `peliculas_ibfk_1` FOREIGN KEY (`id_clave`,`id_tipo`) REFERENCES `material` (`id_clave`, `id_tipo`) ON DELETE CASCADE;

--
-- Filtros para la tabla `prestamos`
--
ALTER TABLE `prestamos`
  ADD CONSTRAINT `prestamos_ibfk_1` FOREIGN KEY (`id_clave`) REFERENCES `material` (`id_clave`),
  ADD CONSTRAINT `prestamos_ibfk_2` FOREIGN KEY (`id_prestador`) REFERENCES `prestadores` (`id_prestador`),
  ADD CONSTRAINT `prestamos_ibfk_3` FOREIGN KEY (`id_usuario`) REFERENCES `usuarios` (`id_usuario`);
COMMIT;

/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
