<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Administracion extends CI_Controller {

	public function __construct(){
		parent::__construct();
	}

	public function index()
	{
		if($this->session->userdata('username')){
			$this->load->view('administracion');
		}else{
			redirect('login#','refresh');
		}
	}
}

/* End of file Controllername.php */
