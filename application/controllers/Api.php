<?php
 
//debemos colocar esta línea para extender de REST_Controller
require_once APPPATH.'/libraries/REST_Controller.php';
class Api extends REST_Controller
{

    public function __construct() {
        parent::__construct();
        
        $this->load->database();
        $this->load->helper('url');
        $this->load->model("api_model");
        $this->load->model("usuarios_model");
        $this->load->model("clasificaciones_model");

        $this->load->model("libros_model");
        $this->load->model("musica_model");
        $this->load->model("videos_model");
        $this->load->model("peliculas_model");
        $this->load->model("Datos_model");
        $this->load->model("prestadores_model");
        $this->load->model("Usuarios_model");
        $this->load->model("Prestamos_model");
        $this->load->model("Prest_lib_model");
        $this->load->model("tipomaterial_model");
        $this->load->model("blueray_model");



    }

///////////////////USUARIOS/////////////////////

public function usuarios_get($id_usuario=null){ //obtener
    if($id_usuario){
        $resultado = $this->usuarios_model->get_usuario($id_usuario);
    }else{
        $resultado = $this->usuarios_model->usuarios();
    }
    if($resultado){
        $this->response($resultado, 200);
    }else{
        $this->response("Error", 400);
    }
}

public function usuarios_post(){ //insertar
    $id_usuario = $this->post('id_usuario'); 
    $nombre = $this->post('nombre');
    $ape_pat = $this->post('ape_pat');
    $ape_mat = $this->post('ape_mat');
    $correo = $this->post('correo');
    $telefono = $this->post('telefono');
    $adeudo = $this->post('adeudo');
    $resultado = $this->usuarios_model->guardar($id_usuario,$nombre,$ape_pat,$ape_mat,$correo,$telefono,$adeudo);
    if($resultado){
        $this->response('Guardado con exito',200);
    }else{
        $this->response('Error',400);
    }
}

public function usuarios_put($id_usuario){   //Actualizar
    $id_usuario = $this->put('id_usuario'); 
    $nombre = $this->put('nombre');
    $ape_pat = $this->put('ape_pat');
    $ape_mat = $this->put('ape_mat');
    $correo = $this->put('correo');
    $telefono = $this->put('telefono');
    $adeudo = $this->put('adeudo');
    $resultado = $this->usuarios_model->actualizar($id_usuario,$nombre,$ape_pat,$ape_mat,$correo,$telefono,$adeudo);
    if($resultado){
        $this->response('Actualizado con exito',200);
    }else{
        $this->response('Error',400);
    }
}

public function usuarios_delete($id_usuario){
        
    $resultado = $this->usuarios_model->eliminar($id_usuario);
    if($resultado){
        $this->response('Eliminado con exito',200);
    }else{
        $this->response('Error',400);
    }
}




////////////////////PRESTADORES///////////////////

public function prestadores_get($id_prestador=null){ //obtener
    if($id_prestador){
        $resultado = $this->prestadores_model->get_prestador($id_prestador);
    }else{
        $resultado = $this->prestadores_model->prestadores();
    }
    if($resultado){
        $this->response($resultado, 200);
    }else{
        $this->response("Error", 400);
    }
}
 
public function prestadores_post(){ //insertar
    $id_prestador = $this->post('id_prestador'); 
    $nombre = $this->post('nombre');
    $ape_pat = $this->post('ape_pat');
    $ape_mat = $this->post('ape_mat');
    $correo = $this->post('correo');
    $telefono = $this->post('telefono');
    $contrasena = $this->post('contrasena');
    $resultado = $this->prestadores_model->guardar($id_prestador,$nombre,$ape_pat,$ape_mat,$correo,$telefono,$contrasena);
    if($resultado){
        $this->response('Guardado con exito',200);
    }else{
        $this->response('Error',400);
    }
}

public function prestadores_delete($id_prestador){
        
    $resultado = $this->prestadores_model->eliminar($id_prestador);
    if($resultado){
        $this->response('Eliminado con exito',200);
    }else{
        $this->response('Error',400);
    }
}

public function prestadores_put($id_prestador){   //Actualizar
    
    $nombre = $this->put('nombre');
    $ape_pat = $this->put('ape_pat');
    $ape_mat = $this->put('ape_mat');
    $correo = $this->put('correo');
    $telefono = $this->put('telefono');
    $contrasena = $this->put('contrasena');
    $resultado = $this->prestadores_model->actualizar($id_prestador,$nombre,$ape_pat,$ape_mat,$correo,$telefono,$contrasena);
    if($resultado){
        $this->response('Actualizado con exito',200);
    }else{
        $this->response('Error',400);
    }
}



    ////////////////Libros///////////////////

    public function libros_get($id_libro=null){ //obtener
        if($id_libro){
            $resultado = $this->libros_model->libro($id_libro);
        }else{
            $resultado = $this->libros_model->libros();
        }
        if($resultado){
            $this->response($resultado, 200);
        }else{
            $this->response("Error", 400);
        }
    }


    public function libros_post(){ //insertar
        $clasificacion = $this->post('clasificacion');
        $autor = $this->post('autor');
        $titulo = $this->post('titulo');
        $pie_imprenta = $this->post('pie_imprenta');
        $paginas = $this->post('paginas');
        $isbn = $this->post('isbn');
        $no_adquisicion=$this->post('no_adquisicion');
        $temas = $this->post('temas');
        $resultado = $this->libros_model->guardar($clasificacion,$autor,$titulo,$pie_imprenta,$paginas,$isbn,$no_adquisicion,$temas);
        if($resultado){
            $this->response('Guardado con exito',200);
        }else{
            $this->response('Error',400);
        }
    }

    //En el post se debe a cambiar al delete y solo se le pasa el parametro de la id que se eliminara
    //http://localhost:8080/ININ/index.php/api/eliminarPelicula/5
    public function libros_delete($id_clave){
        
        $resultado = $this->libros_model->eliminar($id_clave);
        if($resultado){
            $this->response('Eliminado con exito',200);
        }else{
            $this->response('Error',400);
        }
    }

   
    public function libros_put($id_clave){   //actualizar
        $clasificacion = $this->put('clasificacion');
        $autor = $this->put('autor');
        $titulo = $this->put('titulo');
        $pie_imprenta = $this->put('pie_imprenta');
        $paginas = $this->put('paginas');
        $isbn = $this->put('isbn');
        $no_adquisicion = $this->put('no_adquisicion');
        $temas = $this->put('temas');
        $resultado = $this->libros_model->actualizar($id_clave,$titulo,$clasificacion,$autor,$pie_imprenta,$paginas,$isbn,$no_adquisicion,$temas);
        if($resultado){
            $this->response('Actualizado con exito',200);
        }else{
            $this->response('Error',400);
        }
    }

/////////////////MUSICA////////////
    public function musica_get($id_musica = null){
        if($id_musica){
            $resultado =  $this->musica_model->get_musica($id_musica);
        }else{
            $resultado = $this->musica_model->musica();
        }    
        
        $this->response($resultado, 200);
    }

    public function musica_post(){
        $titulo = $this->post('titulo');
        $interprete = $this->post('interprete');
        $clasificacion = $this->post('clasificacion');
        $resultado = $this->musica_model->guardar($titulo,$interprete,$clasificacion);
        if($resultado){
            $this->response('Guardado con exito',200);
        }else{
            $this->response('Error',400);
        }
    }


    public function musica_put($id_musica){
        $titulo = $this->put('titulo');
        $interprete = $this->put('interprete');
        $clasificacion = $this->put('clasificacion');
        //$id_tipo = $this->post('id_tipo');
        //$estado = $this->post('estado');

        $resultado = $this->musica_model->actualizar($id_musica,$titulo, $interprete, $clasificacion);
        if($resultado){
            $this->response($resultado,200);
        }else{
            $this->response($resultado,400);
        }
    }

    public function musica_delete($id_musica){
        $resultado = $this->musica_model->eliminar($id_musica);
        if($resultado){
            $this->response('Eliminado exitosamente', 200);
        }else{
            $this->response('Error', 400);
        }
    }

 //////////////////PELICULAS/////////////////////////
  
public function peliculas_post(){
    $clasificacion = $this->post('clasificacion');
    $titulo = $this->post('titulo');
    $pais = $this->post('pais');
    $anio = $this->post('anio');
    $director = $this->post('director');
    $reparto = $this->post('reparto');
    $calificacion=$this->post('calificacion');
    $region=$this->post('region');

    $resultado=$this->peliculas_model->guardar($clasificacion,$titulo,$pais,$anio,$director,$reparto,$calificacion,$region);
    if($resultado){
        $this->response('Guardado con exito',200);
    }else{
        $this->response('Error',400);
    }
}



    public function peliculas_get($id_clave=null){
        if($id_clave){
            $resultado = $this->peliculas_model->pelicula($id_clave);
        }else{
            $resultado = $this->peliculas_model->peliculas();
        }
        if($resultado){
            $this->response($resultado, 200);
        }else{
            $this->response("Error", 400);
        }
    }

    public function peliculas_put($id_clave){
        $clasificacion = $this->put('clasificacion');
        $titulo = $this->put('titulo');
        $pais = $this->put('pais');
        $anio = $this->put('anio');
        $director = $this->put('director');
        $reparto = $this->put('reparto');
        $calificacion=$this->put('calificacion');
        $region=$this->put('region');
        $resultado = $this->peliculas_model->actualizar($id_clave,$clasificacion,$titulo,$pais,$anio,$director,$reparto,$calificacion,$region);
        if($resultado){
            $this->response('Actualizado con exito',200);
        }else{
            $this->response('Error',400);
        }
    }


    public function peliculas_delete($id_clave){
        
        $resultado = $this->peliculas_model->eliminar($id_clave);
        if($resultado){
            $this->response('Eliminado con exito',200);
        }else{
            $this->response('Error',400);
        }
    }


    ////////////////BLUERAY/////////////////
    public function blueray_post(){
        $clasificacion = $this->post('clasificacion');
        $titulo = $this->post('titulo');
        $pais = $this->post('pais');
        $anio = $this->post('anio');
        $director = $this->post('director');
        $reparto = $this->post('reparto');
        $calificacion=$this->post('calificacion');
        $link=$this->post('link');
        $estado=$this->post('estado');
        $id_tipo=$this->post('id_tipo');
        $resultado=$this->blueray_model->guardar($clasificacion,$titulo,$pais,$anio,$director,$reparto,$calificacion,$link,$id_tipo,$estado);
        if($resultado){
            $this->response('Guardado con exito',200);
        }else{
            $this->response('Error',400);
        }
    }

    public function blueray_get($id_blueray=null){
        if($id_blueray){
            $resultado = $this->blueray_model->id_blueray($id_blueray);
        }else{
            $resultado = $this->blueray_model->get_blueray();
        }
        if($resultado){
            $this->response($resultado, 200);
        }else{
            $this->response("Error", 400);
        }
    }

    public function blueray_put($id_blueray){
        $clasificacion = $this->put('clasificacion');
        $titulo = $this->put('titulo');
        $pais = $this->put('pais');
        $anio = $this->put('anio');
        $director = $this->put('director');
        $reparto = $this->put('reparto');
        $calificacion=$this->put('calificacion');
        $link=$this->put('link');
        $id_tipo=$this->put('id_tipo');
        $estado=$this->put('estado');
        $resultado = $this->blueray_model->actualizar($id_blueray,$clasificacion,$titulo,$pais,$anio,$director,$reparto,$calificacion,$link,$id_tipo,$estado);
        if($resultado){
            $this->response('Actualizado con exito',200);
        }else{
            $this->response('Error',400);
        }
    }


    public function blueray_delete($id_blueray){
        
        $resultado = $this->blueray_model->eliminar($id_blueray);
        if($resultado){
            $this->response('Eliminado con exito',200);
        }else{
            $this->response('Error',400);
        }
    }


 
  



    //fUnciones de abril
    public function prestamos_post(){
        $fecha_prestamo = $this->post('fecha_prestamo');
        $fecha_entrega = $this->post('fecha_prometida');
      
        $id_libro = $this->post('id_clave');
        $id_usuario = $this->post('id_usuario');
        $id_prestador = $this->post('id_prestador');
        $id_prestamo = $this->Prestamos_model->insertarPrestamo($fecha_prestamo,$fecha_entrega,$id_prestador,$id_usuario);
        $resultado=$this->Prest_lib_model->insertarPrestamo($id_prestamo,$id_libro);


        if($resultado){
            $this->response('Guardado con exito',200);
        }else{
            $this->response('Error',400);
        }
    }


    public function materialdisponible_get($tipo,$pagina,$n,$busqueda=null){
        if($tipo){
            $materiales = $this->Prestamos_model->getMaterial($tipo,$pagina,$n,$busqueda);
            $this->response($materiales,200);
        }else $this->response("parametros incompletos",400);
    }

    public function prestamosPor_get($tipo){
        if($tipo){
            $prestamos = $this->Prestamos_model->getPrestamos($tipo);
            $this->response($prestamos,200);
        }else $this->response("Parametros incompletos",400);
    }

    public function datosUsuario_get($id){
        if($id){
            $datosUsuario = $this->Usuarios_model->getData($id);
            $this->response($datosUsuario,200);
        }else $this->response("Parametros incompletos",400);
    }

    public function guardarPrestamos_post(){
     
        $prestamos = $this->post("prestamos");

        $resultado = $this->Prestamos_model->guardarPrestamo($prestamos);
        if($resultado){
            $this->response('Guardado con exito',200);
        }else{
            $this->response('Error',400);
        }
        
    }

    //Funcion para obtener los prestamos
    public function obtenerPrestamos_get($tipo=null){
       
            $prestamos = $this->Prestamos_model->getPrestamosTodos($tipo);
            $this->response($prestamos,200);
      
    }

    //Función para obtener prestamos de un especifico usuario
    public function prestamosUsuario_get($id_usuario){
        $prestamos=$this->Prestamos_model ->getPretamosUsuario($id_usuario);
        $this->response($prestamos,200);
    }


    //*Creo que es la de devolver material
    public function prestamosUsuario_put($id_usuario){
        $id_clave = $this->put('id_clave');
        //$fecha_devolucion = $this->put('fecha_devolucion');
        $resultado = $this->Prestamos_model ->devolverPrestamosTodos($id_usuario);
        if($resultado){
            $this->response($resultado,200);
        }else{
            $this->response($resultado,400);
        }
    }
    //UPDATE prestamos SET fecha_prometida = DATE_ADD(fecha_prometida, INTERVAL 2 DAY) WHERE id_prestamo=73
    //UPDATE transaction SET currentDate = DATE_ADD(currentDate, INTERVAL 13 DAY)
    /*
    public function prestamosUsuario_put(){
        $id_prestamo = $this->put('id_prestamo');
        $id_clave = $this->put('id_clave');
        $fecha_devolucion = $this->put('fecha_devolucion');
        $prestamos=$this->Prestamos_model ->DevolverMaterial($id_prestamo,$id_clave,$fecha_devolucion);
        $this->response($prestamos,200);
    }
    */
   
    //Función para renovar todos los prestamos pasando como parametro el id de usuario
    public function renovarPrestamos_put($id_usuario){
        $resultado = $this->Prestamos_model ->renovarPrestamosTodos($id_usuario);
        if($resultado){
            $this->response($resultado,200);
        }else{
            $this->response($resultado,400);
        }
    }
    //Función para renovar un prestamos en especifico
    public function renovarPrestamo_put($id_prestamo){
        $resultado = $this->Prestamos_model ->renovarPrestamo($id_prestamo);
        if($resultado){
            $this->response($resultado,200);
        }else{
            $this->response($resultado,400);
        }
    }

    //Funcion para el historial de prestamos por usuario
    public function historialPrestamos_get($id_usuario){
        $resultado = $this->Prestamos_model ->historialPrestamos($id_usuario);
        if($resultado){
            $this->response($resultado,200);
        }else{
            $this->response($resultado,400);
        }
    }

    //Nuevas funciones 
    public function prestamosRetrasados_get(){
        $prestamos=$this->Prestamos_model ->getPretamosRetrasados();
        $this->response($prestamos,200);
    }

    public function prestamosDevueltos_get(){
        $prestamos=$this->Prestamos_model ->getPretamosDevueltos();
        $this->response($prestamos,200);
    }

    public function prestamosporFecha_get($fecha_prestamo){
        $prestamos=$this->Prestamos_model ->getPretamosporFecha($fecha_prestamo);
        $this->response($prestamos,200);
    }


    //Prestamos que tienen un usuario en especifico
    /*public function prestamosporUsuario_get($id_usuario){
        $prestamos=$this->Prestamos_model ->getPretamosporFecha($id_usuario);
        $this->response($prestamos,200);
    }
    */

    //Función para devolver el material prestado
    public function devolverPrestamo_put(){
        $id_usuario = $this->put('id_usuario');
      
        $resultado=$this->Prestamos_model->devolverPrestamosTodos($id_usuario);


        if($resultado){
            $this->response('Guardado con exito',200);
        }else{
            $this->response('Error',400);
        }
    }

    public function devolverPrestamoIndividual_put($id_prestamo){
       

        $resultado=$this->Prestamos_model->devolverPrestamoIndividual($id_prestamo);


        if($resultado){
            $this->response('Guardado con exito',200);
        }else{
            $this->response('Error',400);
        }
    }

  
}
