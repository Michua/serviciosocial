<?php

defined('BASEPATH') OR exit('No direct script access allowed');

class Usuarios extends CI_Controller {
    function __construct()
    {
        // Construct the parent class
        parent::__construct();

        // Configure limits on our controller methods
        // Ensure you have created the 'limits' table and enabled 'limits' within application/config/rest.php
        $this->load->model('usuarios_model');
    }

    public function index()
    {
        if($this->session->userdata('correo')){
            $this->load->view('header');
            $this->load->view('usuarios_index');		
            $this->load->view('footer');
		}else{
			redirect('login#InicieSesion','refresh');
		}
    }

    public function nuevo(){
        if(isset($_POST['nombre'])){
            $nombre = $_POST['nombre'];
            $paterno = $_POST['paterno'];
            $materno = $_POST['materno'];
            $correo = $_POST['correo'];
            $telefono = $_POST['telefono'];
            $direccion = $_POST['direccion'];

            
            $resultado = $this->usuarios_model->registrar($nombre,$paterno,$materno,$correo,$telefono,$direccion);
            redirect('usuarios','refresh');
        }
        $this->load->view('usuario_nuevo');
    }

    public function editar($id){
        $data['usuario'] = $this->usuarios_model->usuario($id);

        if(isset($_POST['nombre'])){
            $nombre = $_POST['nombre'];
            $paterno = $_POST['paterno'];
            $materno = $_POST['materno'];
            $correo = $_POST['correo'];
            $telefono = $_POST['telefono'];
            $direccion = $_POST['direccion'];

            
            $resultado = $this->usuarios_model->actualizar($id,$nombre,$paterno,$materno,$correo,$telefono,$direccion);
            redirect('usuarios','refresh');
        }

        $this->load->view('usuario_editar',$data);
    }

}

/* End of file Controllername.php */
