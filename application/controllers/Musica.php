<?php

defined('BASEPATH') OR exit('No direct script access allowed');

class Musica extends CI_Controller {

    public function index()
    {
        
        
        if($this->session->userdata('correo')){
            $this->load->view('header');
            $this->load->view('musica_index');	
            $this->load->view('footer');
		}else{
			redirect('login#InicieSesion','refresh');
		}

    }

}

/* End of file Libros.php */
