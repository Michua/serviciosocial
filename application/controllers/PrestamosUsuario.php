<?php

defined('BASEPATH') OR exit('No direct script access allowed');

class PrestamosUsuario extends CI_Controller {

    public function index()
    {
        
        if($this->session->userdata('correo')){
			$this->load->view('header');
            $this->load->view('prestamosUsuario');	
            $this->load->view('footer');
		}else{
			redirect('login#','refresh');
		}
    }

}

/* End of file Libros.php */
