
<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Login extends CI_Controller {

	public function __construct(){
		parent::__construct();
		$this->load->model("usuarios_model");
	}

	public function index()
	{
		if($this->session->userdata('correo')){
			redirect('inicio','refresh');
		}
		if(isset($_POST['password'])){
			$prestador = $this->usuarios_model->login($_POST['correo'],$_POST['password']);
			
			if($prestador){
				$this->session->set_userdata($prestador);
				redirect('inicio','refresh');
			}else{
				redirect('login#bad-password','refresh');
			}
		
		}
		$this->load->view('login');
	}

	public function logout()
	{
		$this->session->sess_destroy();
		redirect('login','refresh');
		
	}

}

/* End of file Login.php */

