<?php

defined('BASEPATH') OR exit('No direct script access allowed');

class Prestadores extends CI_Controller {

    public function index()
    {
        
        if($this->session->userdata('correo')){
			$this->load->view('header');
            $this->load->view('prestadores_index');	
            $this->load->view('footer');
		}else{
			redirect('login#','refresh');
		}
    }

}