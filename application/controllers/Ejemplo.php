<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Ejemplo extends CI_Controller {

	public function __construct(){
		parent::__construct();
	}

    //Index es la función que carga por defecto
	public function index()
	{
        $this->load->view('ejemploVista');
    }

    //funcion ejemplo de un controlador
	public function nombreDeLaFuncion()
	{
        //Carga la vista ubicada en la carpeta views
        $this->load->view('nombre_de_la_vista');
    }
    

     //Ejemplo de una funcion
	public function nuevo()
	{
        //Carga la vista ubicada en la carpeta views
        $this->load->view('nuevo');
    }
    
}
