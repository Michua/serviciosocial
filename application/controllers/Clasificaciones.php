<?php

defined('BASEPATH') OR exit('No direct script access allowed');

class Clasificaciones extends CI_Controller {

    public function __construct(){
        parent::__construct();
        $this->load->model('clasificaciones_model');
    }
    
    public function musica(){
        $data['tipo'] = 1;
        $data['titulo'] = 'Música';
        $data['funcion'] = 'Musica';
        $data['clase'] = 'fa fa-music';
        $this->load->view('clasificaciones_render_index',$data);
    }

    public function peliculas(){
        $data['tipo'] = 2;
        $data['titulo'] = 'Películas';
        $data['funcion'] = 'Pelicula';
        $data['clase'] = 'fa fa-film';
        $this->load->view('clasificaciones_render_index',$data);
    }

    public function libros(){
        $data['tipo'] = 3;
        $data['titulo'] = 'Libros';
        $data['funcion'] = 'Libro';
        $data['clase'] = 'fa fa-book';
        $this->load->view('clasificaciones_render_index',$data);
    }

    public function nuevoMusica(){
        $data['tipo'] = 1;
        $data['titulo'] = 'Música';
        $data['funcion'] = 'musica';
        $data['clase'] = 'fa fa-music';
        $this->load->view('clasificaciones_form',$data);
    }

    public function nuevoPelicula(){
        $data['tipo'] = 2;
        $data['titulo'] = 'Películas';
        $data['funcion'] = 'peliculas';
        $data['clase'] = 'fa fa-film';
        $this->load->view('clasificaciones_form',$data);
    }

    public function nuevoLibro(){
        $data['tipo'] = 3;
        $data['titulo'] = 'Libros';
        $data['funcion'] = 'libros';
        $data['clase'] = 'fa fa-book';
        $this->load->view('clasificaciones_form',$data);
    }

    public function editar($id){
        $consulta = $this->clasificaciones_model->clasificacion($id);
        $data['nombre'] = $consulta['nombre'];
        $data['tipo'] = $consulta['tipo'];
        $data['id'] = $id;
        if($data['tipo'] == 1){
             $data['titulo'] = 'Música';
            $data['funcion'] = 'musica';
            $data['clase'] = 'fa fa-music';
        }

        if($data['tipo'] == 2){
             $data['titulo'] = 'Películas';
            $data['funcion'] = 'film';
            $data['clase'] = 'fa fa-film';
        }

        if($data['tipo'] == 3){
             $data['titulo'] = 'Libros';
            $data['funcion'] = 'libros';
            $data['clase'] = 'fa fa-book';
        }

        $this->load->view('clasificaciones_form_edit',$data);
    }



}

/* End of file Controllername.php */
