<?php

defined('BASEPATH') OR exit('No direct script access allowed');

class Libros extends CI_Controller {

    public function index()
    {
        if($this->session->userdata('correo')){
			$this->load->view('header');
            $this->load->view('libros_index');	
            $this->load->view('footer');
		}else{
			redirect('login#','refresh');
		}
        
    }

}

/* End of file Libros.php */
