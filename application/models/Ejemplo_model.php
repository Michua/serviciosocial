
<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Ejemplo_model extends CI_Model {

	public function ObtenerAutores($tipo){
		$query = "SELECT * FROM autores WHERE tipo = {$tipo}";
		$resultado = $this->db->query($query);
		if($resultado){
			return $resultado->result_array();
		}
		return false;
	}	

}

/* End of file Autores_model.php */
/* Location: ./application/models/Autores_model.php */