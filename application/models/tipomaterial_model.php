<?php
defined('BASEPATH') OR exit('No direct script access allowed');


class tipomaterial_model extends CI_Model {

      public function tipos(){
		$query = "SELECT * FROM tipomaterial";
		$resultado = $this->db->query($query);
		if($resultado){
			return $resultado->result_array();
		}
		return false;
    }
    
    public function get_tipo($id_tipo){
		$query = "SELECT * FROM tipomaterial WHERE id_tipo=$id_tipo";
		$resultado = $this->db->query($query);
		if($resultado){
			return $resultado->row_array();
		}
		return false;
    }
    
    public function guardar($id_tipo,$tipo){
		$query = "INSERT INTO tipomaterial(id_tipo,tipo) VALUES('{$id_tipo}','{$tipo}')";
        $this->db->query($query);
        if ($this->db->affected_rows() === 1) {
            return true;
        }
        return false;
    }
    
    

}