<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Datos_model extends CI_Model {

	public function libros(){
		$query = "SELECT * FROM libros" ;
		$resultado = $this->db->query($query);
		if($resultado){
			return $resultado->result_array();
		}
		return false;
	}	

	public function libro($id_libro){
		$query = "SELECT * FROM libros WHERE id_libro=($id_libro)" ;
		$resultado = $this->db->query($query);
		if($resultado){
			return $resultado->row_array();
		}
		return false;
	}	

    public function guardar($nombre,$ape_paterno,$ape_materno,$correo,$telefono,$direccion,$contrasenia){
        $query = "INSERT INTO datospersonales(nombre,ape_paterno,ape_materno,correo,telefono,direccion,contrasenia) VALUES('{$nombre}','{$ape_paterno}','{$ape_materno}','{$correo}','{$telefono}','{$direccion}','{$contrasenia}')";
        $this->db->query($query);
        if ($this->db->affected_rows() === 1) {
            return $this->db->insert_id();
        }
        return null;
	}
	
	public function eliminar($id_libro){
        $query = "DELETE FROM libros WHERE id_libro=($id_libro)";
        $this->db->query($query);
        if ($this->db->affected_rows() === 1) {
            return true;
        }
        return false;
	}
	
	
    
    public function actualizar($id_datos,$nombre,$ape_paterno,$ape_materno,$correo,$telefono,$direccion,$contrasenia){
		$query = "UPDATE datospersonales SET nombre = '{$nombre}', ape_paterno='{$ape_paterno}', ape_materno='{$ape_materno}', correo='{$correo}', telefono='{$telefono}', direccion='{$direccion}',contrasenia='{$contrasenia}' WHERE id_datos = {$id_datos}";
        $this->db->query($query);
        if ($this->db->affected_rows() === 1) {
            return true;
        }
        return false;

	}
}