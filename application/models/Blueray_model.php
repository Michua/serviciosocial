<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Blueray_model extends CI_Model {

	
	public function get_blueray(){
		//$query = "SELECT * FROM blueray";
		$query = "SELECT  
		material.id_clave, material.titulo, material.clasificacion, blueray.pais, blueray.anio,blueray.director,blueray.reparto,blueray.calificacion,blueray.link, material.disponible
		FROM material INNER JOIN blueray ON material.id_clave = blueray.id_clave WHERE material.id_tipo=4";
		$resultado = $this->db->query($query);
		if($resultado){
			return $resultado->result_array();
		}
		return false;
    }

    public function id_blueray($id_clave){
		$query = "SELECT  
		material.id_clave, material.titulo, material.clasificacion, blueray.pais, blueray.anio,blueray.director,blueray.reparto,blueray.calificacion,blueray.link, material.disponible
		FROM material INNER JOIN blueray ON material.id_clave = blueray.id_clave WHERE material.id_tipo=4 and material.id_clave = $id_clave";
		$resultado = $this->db->query($query);
		if($resultado){
			return $resultado->row_array();
		}
		return false;
	}
	
	public function guardar($clasificacion,$titulo,$pais,$anio,$director,$reparto,$calificacion,$link,$id_tipo,$estado){
		$query = "INSERT INTO material (id_tipo, titulo,clasificacion) VALUES (4, '$titulo', '$clasificacion')";
        $this->db->query($query);
        $id_clave = $this->db->insert_id();
		$query2 = "INSERT INTO blueray (id_clave, id_tipo, pais,anio,director,reparto,calificacion,link) VALUES ($id_clave, 4, '$pais', '$anio', '$director', '$reparto', '$calificacion', '$link')";
		$this->db->query($query2);
		if ($this->db->affected_rows() === 1) {
            return true;
        }
        return null;
	}

	public function actualizar($id_clave,$clasificacion,$titulo,$pais,$anio,$director,$reparto,$calificacion,$link,$id_tipo,$estado){
		$query = "UPDATE material SET titulo = '$titulo', clasificacion = '$clasificacion'  WHERE id_clave = $id_clave";
        $this->db->query($query);
		$query2 = "UPDATE blueray SET pais = '$pais', anio = '$anio', director = '$director', reparto = '$reparto', calificacion = '$calificacion', link = '$link' WHERE id_clave = $id_clave"; 
		 $this->db->query($query2);
        if ($this->db->affected_rows() > 0) {
            return true;
        }
        return false;

	}

	public function eliminar($id_clave){
		$query = "DELETE FROM blueray where id_clave = $id_clave";
        $resultado = $this->db->query($query);
		$query = "DELETE FROM material where id_clave = $id_clave";
		$resultado = $this->db->query($query);
        if ($this->db->affected_rows() > 0) {
            return true;
        }
        return false;
	}

	

}
