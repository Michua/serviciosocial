
<?php
 
class Api_model extends CI_Model
{
    public function get($id){
        $this->db->select("username,register_date");
        $query = $this->db->get_where("usuarios", array("id" => $id));
        if($query->num_rows() == 1){
            return $query->row();
        }
    }
 
    public function get_all(){
        $query = $this->db->get("usuarios");
        if($query->num_rows() > 0){
            return $query->result();
        }
    }

    public function getUsuarios(){
        $query = $this->db->query("SELECT * FROM usuarios");
        if($query->num_rows() > 0){
            return $query->result_array();
        }
    }

    public function eliminar($tabla,$id){
        $query = "DELETE FROM ".$tabla." where id = ".$id;
        $resultado = $this->db->query($query);
        if ($this->db->affected_rows() === 1) {
            return true;
        }
        return false;
    }

    
}