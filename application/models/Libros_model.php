
<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Libros_model extends CI_Model {

	public function libros(){
		$query = "SELECT material.id_clave, material.titulo, material.disponible, material.clasificacion, libros.autor, libros.pie_imprenta, libros.paginas, libros.isbn, libros.no_adquisicion, libros.temas FROM material INNER JOIN libros ON material.id_clave = libros.id_clave WHERE material.id_tipo = 2";
		$resultado = $this->db->query($query);
		if($resultado){
			return $resultado->result_array();
		}
		return false;
	}	

	public function libro($id_clave){
		$query = "SELECT material.id_clave, material.titulo, material.clasificacion, material.disponible, libros.autor, libros.pie_imprenta, libros.paginas, libros.isbn, libros.no_adquisicion, libros.temas FROM material INNER JOIN libros ON material.id_clave = libros.id_clave WHERE material.id_tipo = 2  and material.id_clave = $id_clave";
		$resultado = $this->db->query($query);
		if($resultado){
			return $resultado->row_array();
		}
		return false;
	}	

	public function guardar($clasificacion,$autor,$titulo,$pie_imprenta,$paginas,$isbn,$no_adquisicion,$temas){
		$query = "INSERT INTO material (id_tipo, titulo,clasificacion) VALUES (2, '$titulo', '$clasificacion')";
        $this->db->query($query);
        $id_clave = $this->db->insert_id();				
		$query2 = "INSERT INTO libros (id_clave, id_tipo,autor,pie_imprenta,paginas,isbn,no_adquisicion,temas) VALUES('$id_clave',2 ,'$autor','$pie_imprenta','$paginas','$isbn','$no_adquisicion','$temas')";
        $this->db->query($query2);
        if ($this->db->affected_rows() > 0) {
            return true;
        }
        return false;
	}
	
	public function eliminar($id_clave){
		$query = "DELETE FROM libros where id_clave = $id_clave";
        $resultado = $this->db->query($query);

		$query = "DELETE FROM material where id_clave = $id_clave";
		$resultado = $this->db->query($query);
		
        if ($this->db->affected_rows() > 0) {
            return true;
        }
        return false;
	}
	
	public function actualizar($id_clave,$titulo,$clasificacion,$autor,$pie_imprenta,$paginas,$isbn,$no_adquisicion,$temas){
		$query = "UPDATE material SET titulo = '$titulo', clasificacion = '$clasificacion'  WHERE id_clave = $id_clave";
		$this->db->query($query);
		$query2 = "UPDATE libros SET autor='$autor', pie_imprenta='$pie_imprenta', paginas='$paginas', isbn='$isbn', no_adquisicion='$no_adquisicion', temas='$temas' WHERE id_clave = $id_clave"; 
        $this->db->query($query2);
        if ($this->db->affected_rows() > 0) {
            return true;
        }
        return false;

	}
}

