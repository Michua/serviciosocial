<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Prestamos_model extends CI_Model {
	

    public function insertarPrestamo($fecha_prestamo,$fecha_entrega,$id_prestador,$id_usuario){
        $query = "INSERT INTO prestamos(fecha_prestamo,fecha_entrega,id_prestador,id_usuario) VALUES('{$fecha_prestamo}','{$fecha_entrega}','{$id_prestador}','{$id_usuario}')";
        $this->db->query($query);
        if ($this->db->affected_rows() === 1) {
            return $this->db->insert_id();
        }
        return null;
	}
	
	public function eliminar($id_prestamo){
        $query = "DELETE FROM prestamos WHERE id_prestamo=($id_prestamo)";
        $this->db->query($query);
        if ($this->db->affected_rows() === 1) {
            return true;
        }
        return false;
	}
	
	
    
    public function actualizar($id_prestamo,$fecha_prestamo,$fecha_entrega,$id_prestador,$id_usuario){
		$query = "UPDATE prestamos SET fecha_prestamo = '{$fecha_prestamo}', fecha_entrega='{$fecha_entrega}', id_prestador='{$id_prestador}', id_usuario='{$id_usuario}' WHERE id_prestamo = {$id_prestamo}";
        $this->db->query($query);
        if ($this->db->affected_rows() === 1) {
            return true;
        }
        return false;

    }
    

    //Abril 2019

    public function getPrestamos($tipo){
        $prestamos = $this->db->query("SELECT m.titulo,p.id_prestador,p.id_usuario,p.id_clave,p.fecha_prestamo,p.fecha_devolucion
            FROM prestamos p inner join material m on m.id_clave = p.id_clave where p.id_clave in
            (SELECT id_tipo from material where id_tipo = $tipo)
            ")->result_array();
        $resultado = array();
        foreach ($prestamos as $prestamo) {
            $id_usuario = $prestamo["id_usuario"];
            $id_prestador = $prestamo["id_prestador"];
            $prestamo["usuario"] = $this->db->query("SELECT nombre,ape_pat,ape_mat from usuarios where id_usuario = $id_usuario")->row_array();
            $prestamo["prestador"] = $this->db->query("SELECT nombre,ape_pat,ape_mat from prestadores where id_prestador = $id_usuario")->row_array();

            $resultado[] = $prestamo;
        }
        return $resultado;
    }

    public function getMaterial($tipo,$pagina,$n,$busqueda){
        $inicio = intval($n)*intval($pagina);
        if($busqueda){
            $materiales = $this->db->query("SELECT id_clave,titulo,clasificacion from material where
            id_tipo = $tipo and disponible = 1 and titulo LIKE '%$busqueda%'limit $n offset $inicio
        ")->result_array();
        $cantidad = intval($this->db->query("SELECT count(*) cantidad from material where
        id_tipo = $tipo and disponible = 1 and titulo LIKE '%$busqueda%'")->row_array()["cantidad"]);
        }else{
            $materiales = $this->db->query("SELECT id_clave,titulo,clasificacion from material where
            id_tipo = $tipo and disponible = 1 limit $n offset $inicio
        ")->result_array();
            $cantidad = intval($this->db->query("SELECT count(*) cantidad from material where
            id_tipo = $tipo and disponible = 1")->row_array()["cantidad"]);
        }
        $resultado = array();
        foreach ($materiales as $material ) {
            $id_clave = $material["id_clave"];
            if(intval($tipo) == 1){
                $material["interprete"] = $this->db->query("SELECT interprete from musica where id_clave = $id_clave")->row_array()["interprete"];
            }

            if(intval($tipo) == 2){
                $material["libro"] = $this->db->query("SELECT autor,isbn from libros where id_clave = $id_clave")->row_array();
            }

            if(intval($tipo) == 3){
                $material["pelicula"] = $this->db->query("SELECT director,anio from peliculas where id_clave = $id_clave")->row_array();
            }
            
            if(intval($tipo) == 4){
                $material["blueray"] = $this->db->query("SELECT director,anio from blueray where id_clave = $id_clave")->row_array();
            }

            $resultado[] = $material;
        }
        return array(
            "resultado" => $resultado,
            "cantidad" => $cantidad
         );
    }


    public function guardarPrestamo($prestamos){
       foreach ($prestamos as $registro) {
            $this->db->insert("prestamos",$registro);
            $id_clave = $registro["id_clave"]; //Material
            $this->db->query("UPDATE material set disponible = 0 where id_clave = $id_clave");
       }
        return true;
    }

    public function getPrestamosTodos($tipo){
        if($tipo){
            $prestamos = $this->db->query("SELECT 
        p.nombre nombre_p,p.ape_mat ape_mat_p,p.ape_pat ape_pat_p,
        u.nombre nombre_u,u.ape_mat ape_mat_u,u.ape_pat ape_pat_u,
        m.*,prestamos.*,date_format(prestamos.fecha_prestamo,'%Y-%m-%d') fecha_chida
        FROM prestamos INNER JOIN material m
        ON prestamos.id_clave=m.id_clave INNER JOIN prestadores p
        ON  prestamos.id_prestador=p.id_prestador INNER JOIN usuarios u
        ON prestamos.id_usuario=u.id_usuario WHERE m.id_tipo=$tipo
       ")->result_array();
        }else{
            $prestamos = $this->db->query("SELECT 
        p.nombre nombre_p,p.ape_mat ape_mat_p,p.ape_pat ape_pat_p,
        u.nombre nombre_u,u.ape_mat ape_mat_u,u.ape_pat ape_pat_u,
        m.*,prestamos.*,date_format(prestamos.fecha_prestamo,'%Y-%m-%d') fecha_chida
        FROM prestamos INNER JOIN material m
        ON prestamos.id_clave=m.id_clave INNER JOIN prestadores p
        ON  prestamos.id_prestador=p.id_prestador INNER JOIN usuarios u
        ON prestamos.id_usuario=u.id_usuario
       ")->result_array();
        }
			return $prestamos;
		
        
    }

    /*
    public function getPretamosUsuario($id_usuario){
        $prestamos = $this->db->query("SELECT 
        p.nombre nombre_p,p.ape_mat ape_mat_p,p.ape_pat ape_pat_p,
        u.nombre nombre_u,u.ape_mat ape_mat_u,u.ape_pat ape_pat_u,
        m.*,prestamos.*
        FROM prestamos INNER JOIN material m
        ON prestamos.id_clave=m.id_clave INNER JOIN prestadores p
        ON  prestamos.id_prestador=p.id_prestador INNER JOIN usuarios u
        ON prestamos.id_usuario=u.id_usuario WHERE u.id_usuario=$id_usuario and prestamos.fecha_devolucion is NULL
       ")->result_array();

       return $prestamos;
    }
*/
//Función para  obtener los prestamos que tiene un usuario en especifico    
public function getPretamosUsuario($id_usuario){
    $prestamos = $this->db->query("SELECT 
    p.nombre nombre_p,p.ape_mat ape_mat_p,p.ape_pat ape_pat_p,
    u.nombre nombre_u,u.ape_mat ape_mat_u,u.ape_pat ape_pat_u,
    m.*,prestamos.*, date_format(prestamos.fecha_prestamo,'%Y-%m-%d') fecha_chida
    FROM prestamos INNER JOIN material m
    ON prestamos.id_clave=m.id_clave INNER JOIN prestadores p
    ON  prestamos.id_prestador=p.id_prestador INNER JOIN usuarios u
    ON prestamos.id_usuario=u.id_usuario WHERE prestamos.id_usuario=$id_usuario and prestamos.fecha_devolucion is NULL
   ")->result_array();
    $usuario = $this->db->query("SELECT * from usuarios where id_usuario = $id_usuario")->row_array();
    return array(
        "prestamos" => $prestamos,
        "usuario" => $usuario
     );
}


    public function DevolverMaterial($id_prestamo,$id_clave,$fecha_devolucion){
        $this->db->query("UPDATE prestamos SET fecha_devolucion='$fecha_devolucion' WHERE id_prestamo=$id_prestamo"); 
        $this->db->query("UPDATE material SET disponible='1' WHERE id_clave=$id_clave"); 
        return true;
    }

    public function getPretamosRetrasados(){
        $prestamos = $this->db->query("SELECT 
        p.nombre nombre_p,p.ape_mat ape_mat_p,p.ape_pat ape_pat_p,
        u.nombre nombre_u,u.ape_mat ape_mat_u,u.ape_pat ape_pat_u,
        m.*,prestamos.*, date_format(prestamos.fecha_prestamo,'%Y-%m-%d') fecha_chida
        FROM prestamos INNER JOIN material m
        ON prestamos.id_clave=m.id_clave INNER JOIN prestadores p
        ON  prestamos.id_prestador=p.id_prestador INNER JOIN usuarios u
        ON prestamos.id_usuario=u.id_usuario WHERE CURDATE() > prestamos.fecha_prometida and prestamos.fecha_devolucion is NULL
       ")->result_array();

       return $prestamos;
    }

    public function getPretamosDevueltos(){
        $prestamos = $this->db->query("SELECT 
        p.nombre nombre_p,p.ape_mat ape_mat_p,p.ape_pat ape_pat_p,
        u.nombre nombre_u,u.ape_mat ape_mat_u,u.ape_pat ape_pat_u,
        m.*,prestamos.*, date_format(prestamos.fecha_prestamo,'%Y-%m-%d') fecha_chida
        FROM prestamos INNER JOIN material m
        ON prestamos.id_clave=m.id_clave INNER JOIN prestadores p
        ON  prestamos.id_prestador=p.id_prestador INNER JOIN usuarios u
        ON prestamos.id_usuario=u.id_usuario WHERE prestamos.fecha_devolucion IS NOT NULL
       ")->result_array();

       return $prestamos;
    }

    public function getPretamosporFecha($fecha_prestamo){
        $prestamos = $this->db->query("SELECT 
        p.nombre nombre_p,p.ape_mat ape_mat_p,p.ape_pat ape_pat_p,
        u.nombre nombre_u,u.ape_mat ape_mat_u,u.ape_pat ape_pat_u,
        m.*,prestamos.*, date_format(prestamos.fecha_prestamo,'%Y-%m-%d') fecha_chida
        FROM prestamos INNER JOIN material m
        ON prestamos.id_clave=m.id_clave INNER JOIN prestadores p
        ON  prestamos.id_prestador=p.id_prestador INNER JOIN usuarios u
        ON prestamos.id_usuario=u.id_usuario WHERE prestamos.fecha_prestamo='$fecha_prestamo'
       ")->result_array();

       return $prestamos;
    }
    
    //Función para renovar todos los prestamos
    public function renovarPrestamosTodos($id_usuario){
        $query = "UPDATE prestamos SET fecha_prometida = DATE_ADD(fecha_prometida, INTERVAL 2 DAY), fecha_devolucion=NULL WHERE id_usuario=$id_usuario and prestamos.fecha_devolucion is NULL";
        $this->db->query($query);
        if ($this->db->affected_rows() > 0) {
            return true;
        }
        return false;
    }

    public function renovarPrestamo($id_prestamo){
        $query = "UPDATE prestamos SET fecha_prometida = DATE_ADD(fecha_prometida, INTERVAL 2 DAY), fecha_devolucion=NULL WHERE id_prestamo=$id_prestamo and prestamos.fecha_devolucion is NULL";
        $this->db->query($query);
        if ($this->db->affected_rows() > 0) {
            return true;
        }
        return false;
    }

    
    public function devolverPrestamosTodos($id_usuario){
        
        $fecha_sola = date("Y-m-d");
        $query = "UPDATE prestamos SET fecha_devolucion = '$fecha_sola' WHERE id_usuario=$id_usuario and prestamos.fecha_devolucion is NULL";
        $this->db->query($query);
        if ($this->db->affected_rows() > 0) {
            return true;
        }
        return false;
    }

    public function devolverPrestamoIndividual($id_prestamo,$id_clave){
        $fecha_sola = date("Y-m-d");
        $query = "UPDATE prestamos SET fecha_devolucion = '$fecha_sola' WHERE id_prestamo=$id_prestamo and prestamos.fecha_devolucion is NULL";
        $this->db->query($query);
        $this->db->query("UPDATE material set disponible = 0 where id_clave = $id_clave");
        if ($this->db->affected_rows() > 0) {
            return true;
        }
        return false;
    }

    //Funcion para el historial de  prestamos por usuario
    
    public function historialPrestamos($id_usuario){
        $prestamos = $this->db->query("SELECT 
        p.nombre nombre_p,p.ape_mat ape_mat_p,p.ape_pat ape_pat_p,
        u.nombre nombre_u,u.ape_mat ape_mat_u,u.ape_pat ape_pat_u,
        m.*,prestamos.*, date_format(prestamos.fecha_prestamo,'%Y-%m-%d') fecha_chida
        FROM prestamos INNER JOIN material m
        ON prestamos.id_clave=m.id_clave INNER JOIN prestadores p
        ON  prestamos.id_prestador=p.id_prestador INNER JOIN usuarios u
        ON prestamos.id_usuario=u.id_usuario WHERE prestamos.id_usuario='$id_usuario'
       ")->result_array();

       return $prestamos;
    }
    
//UPDATE prestamos SET fecha_prometida = DATE_ADD(fecha_prometida, INTERVAL 2 DAY) WHERE id_usuario=1 and prestamos.fecha_devolucion is NULL
    //SELECT m.titulo, m.clasificacion, p.nombre, p.ape_pat, p.ape_mat, u.nombre, u.ape_pat, u.ape_mat, prestamos.fecha_prestamo FROM prestamos prestamos INNER JOIN material m
//ON prestamos.id_clave=m.id_clave INNER JOIN prestadores p
//ON  prestamos.id_prestador=p.id_prestador INNER JOIN usuarios u
//ON prestamos.id_usuario=u.id_usuario
}




//select * from prestamos where CURDATE() > prestamos.fecha_prometida;
//Adeudos retrasados