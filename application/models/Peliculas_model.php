<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Peliculas_model extends CI_Model {

	
	public function peliculas(){
		$query = "SELECT  
		material.id_clave, material.titulo, material.clasificacion, peliculas.pais, peliculas.anio,peliculas.director,peliculas.reparto,peliculas.calificacion,peliculas.region, material.disponible
		FROM material INNER JOIN peliculas ON material.id_clave = peliculas.id_clave WHERE material.id_tipo=3";
		$resultado = $this->db->query($query);
		if($resultado){
			return $resultado->result_array();
		}
		return false;
    }

    public function pelicula($id_clave){
		$query = "SELECT  
		material.id_clave, material.titulo, material.clasificacion, peliculas.pais, peliculas.anio,peliculas.director,peliculas.reparto,peliculas.calificacion,peliculas.region, material.disponible
		FROM material INNER JOIN peliculas ON material.id_clave = peliculas.id_clave WHERE material.id_tipo=4 and material.id_clave = $id_clave";
		$resultado = $this->db->query($query);
		if($resultado){
			return $resultado->row_array();
		}
		return false;
	}

	
	public function guardar($clasificacion,$titulo,$pais,$anio,$director,$reparto,$calificacion,$region){
		$query = "INSERT INTO material (id_tipo, titulo,clasificacion) VALUES (3, '$titulo', '$clasificacion')";
        $this->db->query($query);
        $id_clave = $this->db->insert_id();
		$query2 = "INSERT INTO peliculas (id_clave, id_tipo, pais,anio,director,reparto,calificacion,region) VALUES ($id_clave, 3, '$pais', '$anio', '$director', '$reparto', '$calificacion', '$region')";
		$this->db->query($query2);
		if ($this->db->affected_rows() === 1) {
            return true;
        }
        return null;
	}

	public function actualizar($id_clave,$clasificacion,$titulo,$pais,$anio,$director,$reparto,$calificacion,$region){
		$query = "UPDATE material SET titulo = '$titulo', clasificacion = '$clasificacion'  WHERE id_clave = $id_clave";
		$this->db->query($query);
		$query2 = "UPDATE peliculas SET pais = '$pais', anio = '$anio', director = '$director', reparto = '$reparto', calificacion = '$calificacion', region = '$region' WHERE id_clave = $id_clave"; 
		$this->db->query($query2);
        if ($this->db->affected_rows() > 0) {
            return true;
        }
        return false;

	}

	
	public function eliminar($id_clave){
		$query = "DELETE FROM peliculas where id_clave = $id_clave";
        $resultado = $this->db->query($query);
		$query = "DELETE FROM material where id_clave = $id_clave";
		$resultado = $this->db->query($query);
        if ($this->db->affected_rows() > 0) {
            return true;
        }
        return false;
	}

	

}
