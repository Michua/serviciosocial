<?php   


defined('BASEPATH') OR exit('No direct script access allowed');

class Clasificaciones_model extends CI_Model {

    public function clasificacionesTipo($tipo){
        
        $query = "SELECT * FROM clasificaciones where tipo= {$tipo} ORDER BY nombre";
        $resultado = $this->db->query($query);
        return $resultado->result_array();
    }

    public function guardar($nombre,$tipo){
        $query = "INSERT INTO clasificaciones(nombre,tipo) VALUES({$nombre},{$tipo})";
        $this->db->query($query);
        if ($this->db->affected_rows() === 1) {
            return true;
        }
        return false;
    }

    public function actualizar($id,$nombre){

        $query = "UPDATE clasificaciones SET nombre = '{$nombre}' WHERE id = {$id}";
        $this->db->query($query);
        if ($this->db->affected_rows() === 1) {
            return true;
        }
        return false;
    }

    public function clasificacion($id){
        $query = "SELECT * FROM clasificaciones WHERE id = {$id}";
        $resultado = $this->db->query($query);
        if ($resultado->num_rows() > 0) {
            return $resultado->row_array();
        }
        return false;
    }
}

/* End of file ModelName.php */
