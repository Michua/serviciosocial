
<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Videos_model extends CI_Model {

	public function videos(){
		$query = "SELECT * FROM videos" ;
		$resultado = $this->db->query($query);
		if($resultado){
			return $resultado->result_array();
		}
		return false;
	}	

	public function video($id_video){
		$query = "SELECT * FROM videoa WHERE id_video=($id_video)" ;
		$resultado = $this->db->query($query);
		if($resultado){
			return $resultado->row_array();
		}
		return false;
	}	

	public function guardar($clasificacion,$titulo,$pais,$anio,$director,$reparto,$calificacion){
        $query = "INSERT INTO videos(clasificacion,titulo,pais,anio,director,reparto,calificacion) VALUES('{$clasificacion}','{$titulo}','{$pais}',{$anio},'{$director}','{$reparto}','{$calificacion}')";
        $this->db->query($query);
        if ($this->db->affected_rows() === 1) {
            return $this->db->insert_id();
        }
        return null;
	}
	
	public function eliminar($id_libro){
        $query = "DELETE FROM libros WHERE id_libro=($id_libro)";
        $this->db->query($query);
        if ($this->db->affected_rows() === 1) {
            return true;
        }
        return false;
	}
	
	
    
    public function actualizar($id_video,$clasificacion,$titulo,$pais,$anio,$director,$reparto,$calificacion){
		$query = "UPDATE videos SET clasificacion = '{$clasificacion}', titulo='{$titulo}', pais='{$pais}', anio='{$anio}', director='{$director}', reparto='{$reparto}',calificacion='{$calificacion}' WHERE id_video = {$id_video}";
        $this->db->query($query);
        if ($this->db->affected_rows() === 1) {
            return true;
        }
        return false;

	}
}