<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Prest_lib_model extends CI_Model {

	public function prestamos_libros(){
		$query = "SELECT * FROM prestamos_libros INNER JOIN prestamos ON prestamos_libros.id_prestamo=prestamos.id_prestamo INNER JOIN libros ON  prestamos_libros.id_libro=libros.id_libro";
		$resultado = $this->db->query($query);
		if($resultado){
			return $resultado->result_array();
		}
		return false;
	}	
	

	public function prestamos_libro($id_prestamoLib){
		$query = "SELECT * FROM prestamos_libros INNER JOIN prestamos ON prestamos_libros.id_prestamo=prestamos.id_prestamo INNER JOIN libros ON  prestamos_libros.id_libro=libros.id_libro WHERE id_prestamoLib=$id_prestamoLib" ;
		$resultado = $this->db->query($query);
		if($resultado){
			return $resultado->row_array();
		}
		return false;
	}	

	public function insertarPrestamo($id_prestamo,$id_libro){
        $query = "INSERT INTO prestamos_libros(id_prestamo,id_libro) VALUES('{$id_prestamo}','{$id_libro}')";
        $this->db->query($query);
        if ($this->db->affected_rows() === 1) {
            return $this->db->insert_id();
        }
        return null;
	}

	public function actualizar($id_prestamoLib,$id_prestamo,$id_libro){
		$query = "UPDATE prestamos_libros WHERE id_prestamoLib = {$id_prestamoLib}";
        $this->db->query($query);
        if ($this->db->affected_rows() === 1) {
            return true;
        }
        return false;

	}

	public function eliminar($id_usuario){
		$id_datos = $this->db->query("SELECT id_datos FROM usuarios WHERE id_usuario = $id_usuario")->row_array()['id_datos'];
        $query = "DELETE FROM usuarios WHERE id_usuario=$id_usuario";
		$this->db->query($query);
		
		$query = "DELETE FROM datospersonales WHERE id_datos=$id_datos";
		$this->db->query($query);
        if ($this->db->affected_rows() === 1) {
            return true;
        }
        return false;
    }
}