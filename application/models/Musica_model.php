
<?php
defined('BASEPATH') OR exit('No direct script access allowed');


class Musica_model extends CI_Model {

      public function musica(){
		$query = "SELECT material.id_clave,material.titulo, material.clasificacion, material.disponible, musica.interprete, material.disponible FROM material INNER JOIN musica ON material.id_clave = musica.id_clave WHERE material.id_tipo = 1";
		$resultado = $this->db->query($query);
		if($resultado){
			return $resultado->result_array();
		}
		return false;
    }
    
    public function get_musica($id_clave){
        
		$query = "SELECT material.id_clave,material.titulo, material.clasificacion, material.disponible, musica.interprete, material.disponible FROM material INNER JOIN musica ON material.id_clave = musica.id_clave WHERE material.id_tipo = 1 and material.id_clave = $id_clave";
		
		return $this->db->query($query)->row_array();
		
    }
    
    public function guardar($titulo,$interprete,$clasificacion){
		$query = "INSERT INTO material (id_tipo, titulo,clasificacion) VALUES (1, '$titulo', '$clasificacion')";
        $this->db->query($query);
        $id_clave = $this->db->insert_id();
        $query2 = "INSERT INTO musica (id_clave, id_tipo,interprete) VALUES ($id_clave, 1, '$interprete')";
        $this->db->query($query2);
        if ($this->db->affected_rows()  > 0) {
            return true;
        }
        return false;
    }
    
    public function actualizar($id_clave,$titulo,$interprete,$clasificacion){
		$query = "UPDATE material SET titulo = '$titulo', clasificacion = '$clasificacion'  WHERE id_clave = $id_clave";
        $this->db->query($query);
        $query2 = "UPDATE musica SET interprete = '$interprete' WHERE id_clave = $id_clave";
        $this->db->query($query2);
        if ($this->db->affected_rows() > 0) {
            return true;
        }
        return false;

	}

    public function eliminar($id_clave){
        $query = "DELETE FROM musica where id_clave = $id_clave";
        $resultado = $this->db->query($query);

		$query = "DELETE FROM material where id_clave = $id_clave";
		$resultado = $this->db->query($query);
		
        if ($this->db->affected_rows() > 0) {
            return true;
        }
        return false;
	}

}