<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Prest_lib_model extends CI_Model {

	public function prestamos_musica(){
		$query = "SELECT * FROM prestamos_musica INNER JOIN prestamos ON prestamos_musica.id_prestamo=prestamos.id_prestamo INNER JOIN musica ON  prestamos_musica.id_musica=musica.id_musica";
		$resultado = $this->db->query($query);
		if($resultado){
			return $resultado->result_array();
		}
		return false;
	}	
	

	public function prestamos_musica1($id_prestamoMusic){
		$query = "SELECT * FROM prestamos_musica INNER JOIN prestamos ON prestamos_musica.id_prestamo=prestamos.id_prestamo INNER JOIN musica ON  prestamos_musica.id_musica=musica.id_musica WHERE id_prestamoMusic=$id_prestamoMusic" ;
		$resultado = $this->db->query($query);
		if($resultado){
			return $resultado->row_array();
		}
		return false;
	}	

	public function insertarPrestamo($id_prestamo,$id_musica){
        $query = "INSERT INTO prestamos_musica(id_prestamo,id_musica) VALUES('{$id_prestamo}','{$id_musica}')";
        $this->db->query($query);
        if ($this->db->affected_rows() === 1) {
            return $this->db->insert_id();
        }
        return null;
	}

	public function actualizar($id_prestamoMusic){
		$query = "UPDATE prestamos_musica WHERE id_prestamoMusic = {$id_prestamoMusic}";
        $this->db->query($query);
        if ($this->db->affected_rows() === 1) {
            return true;
        }
        return false;

	}

	public function eliminar($id_usuario){
		$id_datos = $this->db->query("SELECT id_datos FROM usuarios WHERE id_usuario = $id_usuario")->row_array()['id_datos'];
        $query = "DELETE FROM usuarios WHERE id_usuario=$id_usuario";
		$this->db->query($query);
		
		$query = "DELETE FROM datospersonales WHERE id_datos=$id_datos";
		$this->db->query($query);
        if ($this->db->affected_rows() === 1) {
            return true;
        }
        return false;
    }
}