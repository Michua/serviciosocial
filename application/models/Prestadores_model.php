<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Prestadores_model extends CI_Model {

	public function prestadores(){
		$query = "SELECT * FROM prestadores" ;
		$resultado = $this->db->query($query);
		if($resultado){
			return $resultado->result_array();
		}
		return false;
	}	
	

	public function get_prestador($id_prestador){
		$query = "SELECT * FROM prestadores WHERE id_prestador=$id_prestador" ;
		$resultado = $this->db->query($query);
		if($resultado){
			return $resultado->row_array();
		}
		return false;
	}	

	public function guardar($id_prestador,$nombre,$ape_pat,$ape_mat,$correo,$telefono,$contrasena){
        $query = "INSERT INTO prestadores(id_prestador,nombre,ape_pat,ape_mat,correo,telefono,contrasena) VALUES('{$id_prestador}','{$nombre}','{$ape_pat}','{$ape_mat}','{$correo}','{$telefono}','{$contrasena}')";
        $this->db->query($query);
        if ($this->db->affected_rows() === 1) {
			//return $this->db->insert_id();
			return true;
        }
        return null;
	}

	public function actualizar($id_prestador,$nombre,$ape_pat,$ape_mat,$correo,$telefono,$contrasena){
		$query = "UPDATE prestadores SET nombre = '{$nombre}', ape_pat = '{$ape_pat}', ape_mat = '{$ape_mat}', correo = '{$correo}', telefono = '{$telefono}', contrasena = '{$contrasena}' WHERE id_prestador = $id_prestador";
        $this->db->query($query);
        if ($this->db->affected_rows() === 1) {
            return true;
        }
        return false;
	}

	public function eliminar($id_prestador){
        $query = "DELETE FROM prestadores WHERE id_prestador=($id_prestador)";
        $this->db->query($query);
        if ($this->db->affected_rows() === 1) {
            return true;
        }
        return false;
	}

}