<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Usuarios_model extends CI_Model {

	public function usuarios(){
		$query = "SELECT * FROM usuarios" ;
		$resultado = $this->db->query($query);
		if($resultado){
			return $resultado->result_array();
		}
		return false;
	}	
	

	public function get_usuario($id_usuario){
		$query = "SELECT * FROM usuarios WHERE id_usuario=$id_usuario" ;
		$resultado = $this->db->query($query);
		if($resultado){
			return $resultado->row_array();
		}
		return false;
	}	

	public function guardar($id_usuario,$nombre,$ape_pat,$ape_mat,$correo,$telefono,$adeudo){
        $query = "INSERT INTO usuarios(id_usuario,nombre,ape_pat,ape_mat,correo,telefono,adeudo) VALUES('{$id_usuario}','{$nombre}','{$ape_pat}','{$ape_mat}','{$correo}','{$telefono}','{$adeudo}')";
        $this->db->query($query);
        if ($this->db->affected_rows() === 1) {
			//return $this->db->insert_id();
			return true;
        }
        return null;
	}

	public function actualizar($id_usuario,$nombre,$ape_pat,$ape_mat,$correo,$telefono,$adeudo){
		$query = "UPDATE usuarios SET nombre = '{$nombre}', ape_pat = '{$ape_pat}', ape_mat = '{$ape_mat}', correo = '{$correo}', telefono = '{$telefono}', adeudo = '{$adeudo}' WHERE id_usuario = {$id_usuario}";
        $this->db->query($query);
        if ($this->db->affected_rows() === 1) {
            return true;
        }
        return false;
	}

	public function eliminar($id_usuario){
        $query = "DELETE FROM usuarios WHERE id_usuario=($id_usuario)";
        $this->db->query($query);
        if ($this->db->affected_rows() === 1) {
            return true;
        }
        return false;
	}

	public function login($correo, $pass){
		$prestador = $this->db->query("SELECT * FROM prestadores WHERE correo = '$correo' 
		and contrasena = '$pass'")->row_array();

        if($prestador){
			return $prestador;
		}
        return null;
	}

	public function getData($id){
		$usuario = $this->db->query("SELECT * from usuarios where id_usuario = $id")->row_array();
		return $usuario;
	}
	
    
}
