<!doctype html>
<html lang="es">

<head>
    <meta charset="UTF-8">
    <meta name="viewport" content="width=device-width, user-scalable=no, initial-scale=1.0, maximum-scale=1.0, minimum-scale=1.0">
    <meta http-equiv="X-UA-Compatible" content="ie=edge">
    <title>ININ</title>
    <link rel="shortcut icon" type="image/x-icon" href="asset/img/logoSutin.icon">
    <link rel="stylesheet" href="<?=base_url()?>asset/vendor/simple-line-icons/css/simple-line-icons.css">
    <link rel="stylesheet" href="<?=base_url()?>asset/vendor/font-awesome/css/fontawesome-all.min.css">
    <link rel="stylesheet" href="<?=base_url()?>asset/css/styles.css">
    <link rel="stylesheet" href="<?=base_url()?>asset/css/bootstrap.min.css" 
        crossorigin="anonymous">
    <link rel="stylesheet" href="<?=base_url()?>asset/css/bootstrap-vue.css">

    <script src="<?=base_url()?>asset/js/vue.js"></script>
    <script src="<?=base_url()?>asset/js/axios.js"></script>
    <script src="<?=base_url()?>asset/js/polyfill.min.js"></script>
    <script src="<?=base_url()?>asset/js/bootstrap-vue.js"></script>
   

</head>

<body class="sidebar-fixed header-fixed">
    <div class="page-wrapper">
        
<<<<<<< HEAD
        <nav class="navbar page-header" style="margin-top: 0%;height: 15%">

            
            <img style="margin: 0 auto; margin-top: 5px; position: absolute" src="<?=base_url()?>asset/img/logoSutin.jpeg" width="230px" height="94px" alt="logo">
            <div style="margin-left:20%; font-size:200%; font-family:Georgia;font-weight:bold">Sindicato Único de Trabajadores de la Industria Nuclear</div>

            <ul class="navbar-nav ml-auto">
                    

                <li class="nav-item dropdown">
                    <a class="nav-link dropdown-toggle" href="#" role="button" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false">
                        <img src="<?=base_url()?>asset/img/usuario.jpg" class="avatar avatar-sm" alt="logo">
                        <span class="small ml-1 d-md-down-none"><?= $this->session->userdata('correo');?></span>
                    </a>

                    <div class="dropdown-menu dropdown-menu-right">
                        <div class="dropdown-header">Cuenta</div>

                        <a href="<?= site_url('login/logout');?>" class="dropdown-item">
                            <i class="fa fa-lock"></i> Cerrar Sesión
                        </a>
                    </div>
                </li>
            </ul>
        </nav>

        

        <div class="main-container">
            <div class="sidebar">
                <nav class="sidebar-nav">
                    <ul class="nav" style="margin-top: 30%">
                        <li class="nav-title">Menú</li>
                        <li class="nav-item">
                            <a href="<?=site_url('inicio');?>" class="nav-link active">
                                <i class="icon icon-calendar"></i> inicio
                            </a>
                        </li>
                        <li class="nav-item">
                            <a href="<?=site_url('prestamos');?>" class="nav-link active">
                                <i class="icon icon-calendar"></i> Préstamos
                            </a>
                        </li>

                        <li class="nav-item">
                            <a href="<?=site_url('usuarios');?>" class="nav-link">
                                <i class="fas fa-users"></i> Usuarios
                            </a>
                        </li>

                        <li class="nav-item">
                            <a href="<?=site_url('prestadores');?>" class="nav-link">
                                <i class="fas fa-users"></i> Prestadores
                            </a>
                        </li>

                        <li class="nav-item">
                            <a href="<?=site_url('libros');?>" class="nav-link">
                                <i class="fa fa-book"></i> Libros
                            </a>
                        </li>

                        <li class="nav-item">
                            <a href="<?=site_url('musica');?>" class="nav-link">
                                <i class="icon icon-music-tone"></i> Música
                            </a>
                        </li>

                        <li class="nav-item">
                            <a href="<?=site_url('peliculas');?>" class="nav-link">
                                <i class="icon icon-film"></i> Películas
                            </a>
                        </li>

                        <li class="nav-item">
                            <a href="<?=site_url('blueray');?>" class="nav-link">
                                <i class="fa fa-play"></i> Blueray
                            </a>
                        </li>
                        <li class="nav-item">
                            <a href="<?=site_url('devolucion');?>" class="nav-link">    
                                <i class="fas fa-undo-alt"></i>Devolución
                            </a>
                        </li>


                    </ul>
               </nav>
            </div>

            <div class="content">
                <div class="container-fluid">
                
                
                    