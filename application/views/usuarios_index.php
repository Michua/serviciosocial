<div id="app" class="row" style="margin-top: 25px;">
    <div class="col-12">
        <div class="card">
            <div class="card-header bg-light">
                <h1>Usuarios</h1>
            </div>
            <div class="card-body">

                <div class="row" style="margin-bottom: 15px;">
                    <div class="col-5">
                    </div>
                    <div class="col-3">
                        <input type="text" class="form-control" v-model='buscadorUsuario' placeholder="Buscar Usuarios..."
                            aria-desc ribedby="basic-addon1">
                    </div>
                    <div class="col-4">
                        <button class="btn btn-success" data-toggle="modal" data-target="#exampleModalLong" @click="reiniciarModel">Registrar
                        Usuario</button>
                    </div>
                </div>

                <table class="table table-striped col-12">
                <thead>
                        <th>Clave</th>
                        <th>Nombre</th>
                        <th>Apellido Paterno</th>
                        <th>Apellido Materno</th>
                        <th>Correo</th>
                        <th>Teléfono</th>
                        <th>Adeudo</th>

                        
                    </thead>
                    <tbody>
                        <tr v-for="usuario in filtrarUsuario">
                            <td>{{usuario.id_usuario}}</td>
                            <td>{{usuario.nombre}}</td>
                            <td>{{usuario.ape_pat}}</td>
                            <td>{{usuario.ape_mat}}</td>
                            <td>{{usuario.correo}}</td>
                            <td>{{usuario.telefono}}</td>
                            <td>{{usuario.adeudo}}</td>

                            <td>
                                <button class="btn btn-round btn-primary" data-toggle="modal" data-target="#editar"
                                    @click="editarUsuario(usuario)"><span class="fa fa-pencil-alt"></span></button>
                                <button class="btn btn-round btn-danger" data-toggle="modal" data-target="#eliminar"
                                    @click="editarUsuario(usuario)"><span class="fa fa-trash"></span></button>
                            </td>

                        </tr>
                    </tbody>
                </table>


            </div>
        </div>
    </div>

   

    <!-- Modal agregar Usuario -->
    <div class="modal fade" id="exampleModalLong" tabindex="-1" role="dialog" aria-labelledby="exampleModalLongTitle"
        aria-hidden="true">
        <div class="modal-dialog" role="document">
            <div class="modal-content">
                <div class="modal-header">
                    <h5 class="modal-title" id="exampleModalLongTitle">Registrar usuario</h5>
                    <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                        <span aria-hidden="true">&times;</span>
                    </button>
                </div>
              
                <form>
                <div class="modal-body">
                    
                    <label for="">Clave</label>
                    <input type="number" class="form-control" placeholder="Clave" v-model="modelUsuario.id_usuario" required/>
                    <br>
                    <label for="">Nombre</label>
                    <input type="text" class="form-control" placeholder="Nombre" v-model="modelUsuario.nombre" required/>
                    <br>
                    <label for="">Apellido Paterno</label>
                    <input type="text" class="form-control" placeholder="Apellido Paterno" v-model="modelUsuario.ape_pat" required/>
                    <br>
                    <label for="">Apellido Materno</label>
                    <input type="text" class="form-control" placeholder="Apellido Materno" v-model="modelUsuario.ape_mat" required/>
                    <br>
                    <label for="">Correo</label>
                    <input type="email" class="form-control" placeholder="Correo" v-model="modelUsuario.correo" required/>
                    <br>
                    <label for="">Teléfono</label>
                    <input type="number" maxlength="10" class="form-control" placeholder="Teléfono" v-model="modelUsuario.telefono" required/>
                    <div class="alert alert-danger mt-4" v-if="errorsForm.length">
                        <ul>
                            <li v-for="e in errorsForm">{{e}}</li>
                        </ul>
                    </div>           

                </div>
                <div class="modal-footer">
                    <button type="button" class="btn btn-secondary" data-dismiss="modal">Cancelar</button>
                    <button type="button"  @click="validar()" class="btn btn-primary">Registrar
                        Usuario</button>
                </div>
                </form>
            </div>
    
        </div>
    </div>

  

    <!-- Modal editar Usuario -->
    <div class="modal fade" id="editar" tabindex="-1" role="dialog" aria-labelledby="exampleModalLongTitle" aria-hidden="true">
        <div class="modal-dialog" role="document">
            <div class="modal-content">
                <div class="modal-header">
                    <h5 class="modal-title" id="exampleModalLongTitle">Editar Usuario</h5>
                    <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                        <span aria-hidden="true">&times;</span>
                    </button>
                </div>
                <form>
                <div class="modal-body">
                    <label for="">Nombre</label>
                    <input type="text" class="form-control" placeholder="Nombre" v-model="modelUsuario.nombre" required/>
                    <br>
                    <label for="">Apellido Paterno</label>
                    <input type="text" class="form-control" placeholder="Apellido Paterno" v-model="modelUsuario.ape_pat" required/>
                    <br>
                    <label for="">Apellido Materno</label>
                    <input type="text" class="form-control" placeholder="Apellido Materno" v-model="modelUsuario.ape_mat"  required/>
                    <br>                   
                    <label for="">Correo</label>
                    <input type="email" class="form-control" placeholder="Correo" v-model="modelUsuario.correo" required/>  
                    <br>                 
                    <label for="">Teléfono</label>
                    <input type="number" maxlength="10" class="form-control" placeholder="Teléfono" v-model="modelUsuario.telefono" required/>
                    <div class="alert alert-danger mt-4" v-if="errorsForm.length">
                        <ul>
                            <li v-for="e in errorsForm">{{e}}</li>
                        </ul>
                    </div>              
                   

                </div>
                <div class="modal-footer">
                    <button type="button" class="btn btn-secondary" data-dismiss="modal">Cancelar</button>
                    <button type="button" @click="validarEdit()" class="btn btn-primary" >Guardar
                        Cambios</button>
                </div>
                </form>
            </div>
        </div>
    </div>

    <!-- Modal confirmar eliminacion Usuarior-->
    <div class="modal sm" id="eliminar" tabindex="-1" role="dialog" aria-labelledby="eliminarUsuario" aria-hidden="true">
        <div class="modal-dialog" role="document">
            <div class="modal-content">
                <div class="modal-header">
                    <h5 class="modal-title" id="exampleModalLongTitle">Confirmar Eliminación</h5>
                    <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                        <span aria-hidden="true">&times;</span>
                    </button>
                </div>
                <div class="modal-body">
                    <center>¿Está seguro que desea eliminar <b>{{modelUsuario.nombre}}</b>? </center>
                </div>
                <div class="modal-footer">
                    <button type="button" class="btn btn-secondary" data-dismiss="modal">Cancelar</button>
                    <button type="button" class="btn btn-danger" data-dismiss="modal" @click="eliminarUsuario(modelUsuario.id_usuario)">Eliminar</button>
                </div>
            </div>
        </div>
    </div>


</div>

<script>
    new Vue({
        el: '#app',
        data: {
            usuario: [],
            buscadorUsuario: '',
            saludo: 'hola :)',
            modelUsuario: {
                id_usuario: "",
                nombre: "",
                ape_pat: "",
                ape_mat:"",
                correo: "",
                telefono: "",
                adeudo: ""
            },
            errorsForm: []
        },
        mounted: function () {
            this.getUsuario();
        },
        methods: {
            editarUsuario(usuario) {
                this.errorsForm = [];
                this.modelUsuario = Object.assign({}, usuario);
            },
            eliminarUsuario(id) {
                var url = "<?=base_url()?>index.php/api/usuarios/" + id;

                axios.delete(url)
                    .then(response => {
                        // alert('Examen borrado');
                        this.getUsuario();
                        this.reiniciarModel();

                    }).catch(error => {
                        console.log(error);
                        this.getUsuario();
                    });

            },
            getUsuario() {
                var url = "<?=base_url()?>index.php/api/usuarios";
                axios.get(url).then(
                    response => {
                        this.usuario = response.data;
                        this.reiniciarModel();

                    }
                ).catch(
                    error => {
                        //alert('Error');
                        this.usuario = [];
                    }
                );
            },
            registar() {
                console.log(this.modelUsuario);
                var url = "<?=base_url()?>index.php/api/usuarios";
                var data = {
                    id_usuario: this.modelUsuario.id_usuario,
                    nombre: this.modelUsuario.nombre,
                    ape_pat: this.modelUsuario.ape_pat,
                    ape_mat: this.modelUsuario.ape_mat,
                    correo: this.modelUsuario.correo,
                    telefono: this.modelUsuario.telefono,
                    adeudo: this.modelUsuario.adeudo
                };
                axios.post(url, data).then(
                    response => {
                        this.getUsuario();
                        this.reiniciarModel();
                    }
                ).catch(
                    error => {
                        console.log(error);
                        alert('Error llena campos');

                    }
                );
            },
            guardarCambios() {
                var url = "<?=base_url()?>index.php/api/usuarios/" + this.modelUsuario.id_usuario;
                var data = {
                    id_usuario: this.modelUsuario.id_usuario,
                    nombre: this.modelUsuario.nombre,
                    ape_pat: this.modelUsuario.ape_pat,
                    ape_mat: this.modelUsuario.ape_mat,
                    correo: this.modelUsuario.correo,
                    telefono: this.modelUsuario.telefono,
                    adeudo: this.modelUsuario.adeudo

                };
                axios.put(url, data).then(
                    response => {
                        this.getUsuario();
                        this.reiniciarModel();
                    }
                );
            },
            reiniciarModel() {
                this.modelUsuario = {
                    nombre: "",
                    ape_pat: "",
                    ape_mat: "",
                    correo: "",
                    telefono: "",
                    adeudo: ""
                }
            },

             validar(){
                
                ok = true;
                this.errorsForm = [];

                if(!this.modelUsuario.id_usuario){
                    this.errorsForm.push("Clave requerida");
                    ok = false;
                }
                if(!this.modelUsuario.nombre){
                    this.errorsForm.push("Nombre requerido");
                    ok = false;
                }
                if(!this.modelUsuario.ape_pat){
                    this.errorsForm.push("Apellido Paterno requerido");
                    ok = false;
                }
                if(!this.modelUsuario.ape_mat){
                    this.errorsForm.push("Apellido Materno requerido");
                    ok = false;
                }
                if(!this.modelUsuario.correo){
                    this.errorsForm.push("Correo requerido");
                    ok = false;
                }
                if(!this.modelUsuario.telefono){
                    this.errorsForm.push("Teléfono requerido");
                    ok = false;
                }
                if(this.modelUsuario.telefono.length>10){
                    this.errorsForm.push("Máximo 10 dígitos en teléfono");
                    ok = false; 
                }


                if(ok){
                    this.registar();
                    $('#nuevo').modal('hide');
                }
            },

            validarEdit(){
                
                ok = true;
                this.errorsForm = [];
                if(!this.modelUsuario.id_usuario){
                    this.errorsForm.push("Clave requerida");
                    ok = false;
                }
                if(!this.modelUsuario.nombre){
                    this.errorsForm.push("Nombre requerido");
                    ok = false;
                }
                if(!this.modelUsuario.ape_pat){
                    this.errorsForm.push("Apellido Paterno requerido");
                    ok = false;
                }
                if(!this.modelUsuario.ape_mat){
                    this.errorsForm.push("Apellido Materno requerido");
                    ok = false;
                }
                if(!this.modelUsuario.correo){
                    this.errorsForm.push("Correo requerido");
                    ok = false;
                }
                if(!this.modelUsuario.telefono){
                    this.errorsForm.push("Teléfono requerido");
                    ok = false;
                }
                if(this.modelUsuario.telefono.length>10){
                    this.errorsForm.push("Máximo 10 dígitos en teléfono");
                    ok = false; 
                }


                if(ok){
                    this.guardarCambios();
                    $('#editar').modal('hide');
                }
            }

        },
        computed: {
            filtrarUsuario: function () {
                if (this.buscadorUsuario === '') {
                    return this.usuario;
                } else {
                    return this.usuario.filter((usuario) => {
                        return (usuario.nombre.toString() + usuario.ape_pat.toString() + usuario.ape_mat
                            .toString()).toLowerCase().includes(this.buscadorUsuario
                            .toLowerCase());
                    });
                }
            }

        }



    });

    /*  $(document).ready(function(){
         $('#tabla-usuarios').DataTable();
     });
       */
</script>