<linck rel="stylesheet" type="text/css" href=estilos.ss>
<div id="app" class="row" style="margin-top: 25px;">
    <div class="col-12">
        <div class="card">
            <div class="card-header bg-light">
                <div class="col-sm-12">
                    <div class="col-sm-6">
                        <h1>Préstamos</h1>
                    </div>
                    <div class="col-sm-6" style="margin-top: 2%; width: 40%; margin-left: 10%">
                        <label>Búsqueda por fecha:</label>
                        <input type="date" v-model="fechaDevolucion" class="form-control">
                        <br>
                        <button class="btn btn-success" style="margin-left: 80%" @click="buscarPrestamosFecha()">Buscar</button>
                    </div>
                </div>
            </div>
            <div class="card-body">

                <div class="row" style="margin-bottom: 15px;">
                    <div class="col-sm-12">
                        <div class="col-sm-6">
                            <select @change="getPrestamos" v-model="tipoMaterial">
                                <option value="" disabled selected>Selecciona tipo de material</option>
                                <option value="">Todos</option>
                                <option value="1">Música</option>
                                <option value="2">Libros</option>
                                <option value="3">Películas</option>
                                <option value="4">BlueRay</option>
                            </select>
                        </div>
                        <div class="col-sm-3" color:red>
                            <button class="btn btn-success" @click="getPrestamosRetrasados">Préstamos
                                Retrasados</button>

                        </div>
                        
                        <div class="col-sm-3">
                            <button class="btn btn-success" @click="getPrestamosDevueltos">Préstamos Devueltos</button>

                        </div>
                    </div>
                </div>

                <table class="table table-striped col-12">
                    <thead>
                        <th>Título</th>
                        <th>Clasificación</th>
                        <th>Prestador</th>

                        <th>Usuario</th>

                        <th>Fecha de préstamo</th>
                        <th>Fecha prometida de devolución</th>
                        <th>Tipo</th>
                    </thead>
                    <tbody>
                        <tr v-for="prestamo in prestamos" >
                            <td>{{prestamo.titulo}}</td>
                            <td>{{prestamo.clasificacion}}</td>
                            <td>{{prestamo.nombre_p}} {{prestamo.ape_pat_p}} {{prestamo.ape_mat_p}}</td>
                            <td>{{prestamo.nombre_u}} {{prestamo.ape_pat_u}} {{prestamo.ape_mat_u}}</td>
                            <td>{{prestamo.fecha_chida}}</td>
                            <td> {{prestamo.fecha_prometida}}</td>
                            <td>{{getTipo(prestamo.id_tipo)}}</td>
                            
                                
                        </td>

                        </tr>
                    </tbody>
                </table>


            </div>
        </div>
    </div>
</div>

<script>
    new Vue({
        el: '#app',
        data: {
            tipoMaterial: "",
            prestamos: [],
            fechaDevolucion: "",
            prestamoDisponible: []

        },
        mounted: function () {
            this.getPrestamos();
        },
        methods: {
            getPrestamos() {
                axios.get("<?=base_url()?>index.php/api/obtenerPrestamos/" + this.tipoMaterial).then(
                    response => {
                        this.prestamos = response.data;
                    }
                )
            },
            getTipo(tipo) {
                if (tipo === '1') return "Música";
                if (tipo === '2') return "Libro";
                if (tipo === '3') return "Película";
                if (tipo === '4') return "BlueRay";
            },
            getPrestamosRetrasados() {
                axios.get("<?=base_url()?>index.php/api/prestamosRetrasados").then(
                    response => {
                        this.prestamos = response.data;
                    }
                )
            },
            getPrestamosDevueltos() {
                axios.get("<?=base_url()?>index.php/api/prestamosDevueltos").then(
                    response => {
                        this.prestamos = response.data;
                    }
                )
            },
            buscarPrestamosFecha() {
                    axios.get("api/prestamosporFecha/" + this.fechaDevolucion)
                        .then(response => {
                            this.prestamos = response.data;
                            this.fechaDevolucion= "";
                        }).catch(
                            error => {
                                alert("Prestamos no encontrados");
                            }
                        )
            },
        },




    });



    /*  $(document).ready(function(){
         $('#tabla-usuarios').DataTable();
     });
       */
</script>