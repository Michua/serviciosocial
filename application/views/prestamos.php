<div id="app" class="row" style="margin-top: 25px;">
    <div class="col-12">
        <div class="card">
            <div class="card-header bg-light">
                <h1>Préstamos</h1>
            </div>
            <div class="card-body">

                <div class="row" style="margin-bottom: 15px;">
                
                </div>

                <table class="table table-striped col-12">
                    <thead>
                        <th>Título</th>
                        <th>Clasificación</th>
                        <th>Nombre</th>
                        <th>Imprenta</th>
                        <th>Páginas</th>
                        <th>ISBN</th>
                        <th>Temas</th>
                        <th>Acciones</th>
                    </thead>
                    <tbody>
                        <tr v-for="prestamo in prestamos">
                            <td>{{prestamo.titulo}}</td>
                            <td>{{prestamo.clasificacion}}</td>
                            <td>{{prestamo.nombre}}</td>
                            <td>{{prestamo.ape_pat}}</td>
                            <td>{{prestamo.ape_mat}}</td>
                     
                           
                        </tr>
                    </tbody>
                </table>


            </div>
        </div>
    </div>
</div>

<script>
    new Vue({
        el: '#app',
        data: {
            prestamos: [],
            tipo: "",
            prestamoDisponible: [],
            prestamo: {
                titulo: "",
                clasificacion: "",
                nombre: "",
                ape_pat: "",
                ape_mat: "",
                fecha_prestamo: "",
                fecha_devolucion:""
            }
        },
        mounted: function () {
            this.getPrestamos();
        },
        methods: {
            getPrestamos() {
                axios.get("<?=base_url()?>index.php/api/obtenerPrestamos").then(
                    response => {
                        this.prestamos = response.data;
                    }
                )
            },
        },
        



    });

    /*  $(document).ready(function(){
         $('#tabla-usuarios').DataTable();
     });
       */
</script>



//PRestamos con barra de selección

<div id="app" class="row" style="margin-top: 25px;">
        <div class="col-12">
            <div class="card">
                <div class="card-header bg-light">
                    <h1>Préstamos</h1>
                </div>
                <div class="card-body">
                        <div class="row" style="margin-bottom: 15px;">
                                <div class="col-5">
                                    <div class="col-sm-12">
                                        <select @change="getData" v-model="tipoMaterial">
                                            <option value="" disabled selected>Selecciona tipo de material</option>
                                            <option value="1">Música</option>
                                            <option value="2">Libros</option>
                                            <option value="3">Películas</option>
                                            <option value="4">BlueRay</option>
                                        </select>
                                    </div>
                                </div>
                                <div class="col-3">
                                    <input type="text" class="form-control" v-model='buscador' placeholder="Buscar prestamo..."
                                        aria-desc ribedby="basic-addon1">
                                </div>
                                <div class="col-4">
                                    <button class="btn btn-success" data-toggle="modal" data-target="#nuevo"
                                        @click="reiniciarModel">Registrar
                                        Préstamo</button>
                                </div>
                            </div>
                    <div class="row" style="margin-bottom: 15px;">
    
                    </div>
    
                    <table class="table table-striped col-12">
                        <thead>
                            <th>Título</th>
                            <th>Clasificación</th>
                            <th>Nombre</th>
                            <th>Imprenta</th>
                            <th>Páginas</th>
                            <th>ISBN</th>
                            <th>Temas</th>
                            <th>Acciones</th>
                        </thead>
                        <tbody>
                            <tr v-for="prestamo in prestamos">
                                <td>{{prestamo.titulo}}</td>
                                <td>{{prestamo.clasificacion}}</td>
                                <td>{{prestamo.nombre}}</td>
                                <td>{{prestamo.ape_pat}}</td>
                                <td>{{prestamo.ape_mat}}</td>
    
    
                            </tr>
                        </tbody>
                    </table>
    
    
                </div>
            </div>
        </div>
    </div>
    
    <script>
        new Vue({
            el: '#app',
            data: {
                prestamos: [],
                prestamoDisponible: [],
                prestamo: {
                    titulo: "",
                    clasificacion: "",
                    nombre: "",
                    ape_pat: "",
                    ape_mat: "",
                    fecha_prestamo: "",
                    fecha_devolucion: ""
                },
               
            },
            mounted: function () {
                this.getPrestamos();
            },
            methods: {
                getPrestamos() {
                    axios.get("<?=base_url()?>index.php/api/obtenerPrestamos").then(
                        response => {
                            this.prestamos = response.data;
                        }
                    )
                },
            },
    
    
    
    
        });
    
        /*  $(document).ready(function(){
             $('#tabla-usuarios').DataTable();
         });
           */
    </script>