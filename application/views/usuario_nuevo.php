<?php $this->load->view('header-admin');?>
   <div id="app">
        <div class="col-md-12" style="padding-top: 4%">
            <div class="col-md-12 panel">
            <div class="col-md-12 panel-heading">
                <h4>Nuevo Usuario</h4>
            </div>
            <div class="col-md-12 panel-body" style="padding-bottom:30px;">
                <div class="col-md-12">
                <form class="cmxform" id="signupForm" method="POST" action="">
                    <div class="col-md-6">
                    <div class="form-group form-animate-text" style="margin-top:40px !important;">
                        <input type="text" class="form-text" id="nombre" name="nombre" required>
                        <span class="bar"></span>
                        <label>Nombre(s)</label>
                    </div>

                    <div class="form-group form-animate-text" style="margin-top:40px !important;">
                        <input type="text" class="form-text" id="paterno" name="paterno" required>
                        <span class="bar"></span>
                        <label>Apellido Paterno</label>
                    </div>

                    <div class="form-group form-animate-text" style="margin-top:40px !important;">
                        <input type="text" class="form-text" id="materno" name="materno" required>
                        <span class="bar"></span>
                        <label>Apellido Materno</label>
                    </div>

                    
                    </div>

                    <div class="col-md-6">
                        <div class="form-group form-animate-text" style="margin-top:40px !important;">
                            <input type="email" class="form-text" id="correo" name="correo" required>
                            <span class="bar"></span>
                            <label>Correo Electrónico</label>
                        </div>

                        <div class="form-group form-animate-text" style="margin-top:40px !important;">
                            <input type="number" class="form-text" id="telefono" name="telefono" required>
                            <span class="bar"></span>
                            <label>Teléfono</label>
                        </div>
                        
                        <div class="form-group form-animate-text" style="margin-top:40px !important;">
                            <input type="text" class="form-text" id="direccion" name="direccion" required>
                            <span class="bar"></span>
                            <label>Dirección</label>
                        </div>

                    </div>                   
                    <div class="col-md-12">
                        <input class="submit btn btn-round btn-success" type="submit" value="Registrar">
                        <a class="button btn btn-round btn-primary" href="<?= site_url('usuarios');?>">Cancelar</a>
                    </div>
                </form>
            </div>
            </div>
        </div>
        </div>
       
    </div>
    <script>
    </script>
<?php $this->load->view('footer-admin');?>