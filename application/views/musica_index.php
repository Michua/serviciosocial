<div id="app" class="row" style="margin-top: 25px;">
    <div class="col-12">
        <div class="card">
            <div class="card-header bg-light">
                <h1>Música</h1>
            </div>
            <div class="card-body">

                <div class="row" style="margin-bottom: 15px;">
                    <div class="col-5">
                    </div>
                    <div class="col-3">
                        <input type="text" class="form-control" v-model='buscador' placeholder="Buscar Disco..."
                            aria-desc ribedby="basic-addon1">
                    </div>
                    <div class="col-4">
                        <button class="btn btn-success" data-toggle="modal" data-target="#nuevo" @click="reiniciarModel">Registrar
                            Disco</button>
                    </div>
                </div>

                <table class="table table-striped col-12">
                    <thead>

                        <th>Título</th>
                        <th>Intérprete</th>
                        <th>Clasificación</th>
                        <th>Estado</th>
                        <th>Acciones</th>
                    </thead>
                    <tbody>
                        <tr v-for="cd in filtro">

                            <td>{{cd.titulo}}</td>
                            <td>{{cd.interprete}}</td>
                            <td>{{cd.clasificacion}}</td>
                            <td>{{getEstado(cd.disponible)}}</td>
                            <td>
                                <button class="btn btn-round btn-primary" data-toggle="modal" data-target="#editar"
                                    @click="guardarEnModel(cd)"><span class="fa fa-pencil-alt"></span></button>
                                <button class="btn btn-round btn-danger" data-toggle="modal" data-target="#eliminar"
                                    @click="guardarEnModel(cd)"><span class="fa fa-trash"></span></button>
                            </td>

                        </tr>
                    </tbody>
                </table>


            </div>
        </div>
    </div>


    <!-- Modal agregar musica -->
    <div class="modal fade" id="nuevo" tabindex="-1" role="dialog" aria-labelledby="exampleModalLongTitle" aria-hidden="true">
        <div class="modal-dialog" role="document">
            <div class="modal-content">
                <div class="modal-header">
                    <h5 class="modal-title" id="exampleModalLongTitle">Registrar disco</h5>
                    <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                        <span aria-hidden="true">&times;</span>
                    </button>
                </div>
                <form >
                <div class="modal-body">
                    
                    <label for="">Título</label>
                    <input type="text" id="titulo" name="titulo" class="form-control" placeholder="Título" v-model="musica.titulo">
                    <br>
                    <label for="">Intérprete</label>
                    <input type="text" class="form-control" placeholder="Intérprete" v-model="musica.interprete" required>
                    <br>
                    <label for="">Clasificación</label>
                    <input type="text" class="form-control" placeholder="Clasificación" v-model="musica.clasificacion" required>
                    <div class="alert alert-danger mt-4" v-if="errorsForm.length">
                        <ul>
                            <li v-for="e in errorsForm">{{e}}</li>
                        </ul>
                    </div>
                </div>
              
                <div class="modal-footer">
                    <button type="button" class="btn btn-secondary" data-dismiss="modal">Cancelar</button>
                    <button  type="button" @click="validar()"  class="btn btn-primary" >Registrar
                        Música</button>
                </div>
              </form>
            </div>
        </div>
    </div>

    <!-- Modal editar musica -->
    <div class="modal fade" id="editar" tabindex="-1" role="dialog" aria-labelledby="exampleModalLongTitle" aria-hidden="true">
        <div class="modal-dialog" role="document">
            <div class="modal-content">
                <div class="modal-header">
                    <h5 class="modal-title" id="exampleModalLongTitle">Editar disco</h5>
                    <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                        <span aria-hidden="true">&times;</span>
                    </button>
                </div>
                <div class="modal-body">

                        <label for="">Título</label>
                        <input type="text" class="form-control" placeholder="Titulo" v-model="musica.titulo" required>
                        <br>
                        <label for="">Intérprete</label>
                        <input type="text" class="form-control" placeholder="Autor" v-model="musica.interprete" required>
                        <br>
                        <label for="">Clasificación</label>
                        <input type="text" class="form-control" placeholder="Clasificación" v-model="musica.clasificacion" required>                           

                        <div class="alert alert-danger mt-4" v-if="errorsForm.length">
                            <ul>
                                <li v-for="e in errorsForm">{{e}}</li>
                            </ul>
                        </div>

                </div>
                <div class="modal-footer">
                    <button type="button" class="btn btn-secondary" data-dismiss="modal">Cancelar</button>
                    <button type="button"  @click="validarEdit()" class="btn btn-primary">Guardar
                        Cambios</button>
                </div>
            </div>
        </div>
    </div>

    <!-- Modal confirmar eliminacion -->
    <div class="modal sm" id="eliminar" tabindex="-1" role="dialog" aria-labelledby="eliminarUsuario" aria-hidden="true">
        <div class="modal-dialog" role="document">
            <div class="modal-content">
                <div class="modal-header">
                    <h5 class="modal-title" id="exampleModalLongTitle">Confirmar Eliminación</h5>
                    <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                        <span aria-hidden="true">&times;</span>
                    </button>
                </div>
                <div class="modal-body">
                    <center>¿Está seguro que desea eliminar <b>{{musica.titulo}}</b>? </center>
                </div>
                <div class="modal-footer">
                    <button type="button" class="btn btn-secondary" data-dismiss="modal">Cancelar</button>
                    <button type="button" class="btn btn-danger" data-dismiss="modal" @click="eliminar(musica.id_clave)">Eliminar</button>
                </div>
            </div>
        </div>
    </div>

</div>

<script>


    new Vue({
        el: '#app',
        data: {
            discos: [],
            buscador: '',
            musica: {
                titulo: "",
                interprete: "",
                clasificacion: "",
                disponible: ""
            },
            errorsForm: []
        },
        mounted: function () {
            this.getData();
        },
        methods: {

            getEstado(estado){
                var estado = parseInt(estado);
                if(estado == 1) return "Disponible";
                else return "Prestado";
            },

            guardarEnModel(item) {
                this.errorsForm = [];
                this.musica = Object.assign({}, item);
            },
            eliminar(id) {
                var url = "<?=base_url()?>index.php/api/musica/" + id;

                axios.delete(url)
                    .then(response => {
                        // alert('Examen borrado');
                        this.getData();
                        this.reiniciarModel();

                    }).catch(error => {
                        console.log(error);
                        this.getData();
                    });

            },
            getData() {
                var url = "<?=base_url()?>index.php/api/musica";
                axios.get(url).then(
                    response => {
                       this.discos = response.data;
                      
                        this.reiniciarModel();
                        
                      
                    }
                ).catch(
                    error => {
                        //alert('Error');
                        this.discos = [];
                    }
                );
            },
            registarMusica() {
   
                var url = "<?=base_url()?>index.php/api/musica";
                var data = {
                    titulo: this.musica.titulo,
                    interprete: this.musica.interprete,
                    clasificacion: this.musica.clasificacion
                };
                axios.post(url, data).then(
                    response => {
                        this.getData();
                        this.reiniciarModel();
                    }
                ).catch(
                    error => {
                        alert('Error');

                    }
                );
            },
            guardarCambios() {
                var url = "<?=base_url()?>index.php/api/musica/" + this.musica.id_clave;
                var data = {
                    titulo: this.musica.titulo,
                    interprete: this.musica.interprete,
                    clasificacion: this.musica.clasificacion
                };
                axios.put(url, data).then(
                    response => {
                        this.getData();
                        this.reiniciarModel();
                    }
                ).catch(
                    error => {
                        console.log(error);
                        alert('Error');
                    }
                );
            },
            reiniciarModel() {
                this.musica = {
                    titulo: "",
                    interprete: "",
                    clasificacion: ""
                };
            },
            validar(){
                
                ok = true;
                this.errorsForm = [];

                if(!this.musica.titulo){
                    this.errorsForm.push("Título requerido");
                    ok = false;
                }
                if(!this.musica.clasificacion){
                    this.errorsForm.push("Clasificación requerido");
                    ok = false;
                }
                if(!this.musica.interprete){
                    this.errorsForm.push("Intérprete requerido");
                    ok = false;
                }


                if(ok){
                    this.registarMusica();
                    $('#nuevo').modal('hide');
                }
            },

            validarEdit(){
                
                ok = true;
                this.errorsForm = [];

                if(!this.musica.titulo){
                    this.errorsForm.push("Título requerido");
                    ok = false;
                }
                if(!this.musica.clasificacion){
                    this.errorsForm.push("Clasificación requerido");
                    ok = false;
                }
                if(!this.musica.interprete){
                    this.errorsForm.push("Intérprete requerido");
                    ok = false;
                }


                if(ok){
                    this.guardarCambios();
                    $('#editar').modal('hide');
                }
            }

        },
        computed: {
            filtro: function () {
                if (this.buscador === '') {
                    return this.discos;
                } else {
                    return this.discos.filter((item) => {
                        return (item.titulo.toString() + item.interprete.toString() + item.clasificacion
                            .toString()).toLowerCase().includes(this.buscador
                            .toLowerCase());
                    });
                }
            }

        }



    });

    
</script>
 