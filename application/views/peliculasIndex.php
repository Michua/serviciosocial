<div id="app" class="row" style="margin-top: 25px;">
    <div class="col-12">
        <div class="card">
            <div class="card-header bg-light">
                <h1>Películas</h1>
            </div>
            <div class="card-body">

                <div class="row" style="margin-bottom: 15px;">
                    <div class="col-5">
                    </div>
                    <div class="col-3">
                        <input type="text" class="form-control" v-model='buscador' placeholder="Buscar Película..."
                            aria-desc ribedby="basic-addon1">
                    </div>
                    <div class="col-4">
                        <button class="btn btn-success" data-toggle="modal" data-target="#nuevo" @click="reiniciarModel">Registrar
                            Película</button>
                    </div>
                </div>

                <table class="table table-striped col-12">
                    <thead>

                        <th>Título</th>
                        <th>Clasificación</th>
                        <th>País</th>
                        <th>Año</th>
                        <th>Director</th>
                        <th>Reparto</th>
                        <th>Calificación</th>
                        <th>Región</th>
                        <th>Estado</th>
                        <th>Acciones</th>
                    </thead>
                    <tbody>
                        <tr v-for="pelicula in filtro">

                            <td>{{pelicula.titulo}}</td>
                            <td>{{pelicula.clasificacion}}</td>
                            <td>{{pelicula.pais}}</td>
                            <td>{{pelicula.anio}}</td>
                            <td>{{pelicula.director}}</td>
                            <td>{{pelicula.reparto}}</td>
                            <td>{{pelicula.calificacion}}</td>
                            <td>{{pelicula.region}}</td>
                            <td>{{getEstado(pelicula.disponible)}}</td>
                            <td>
                                <button class="btn btn-round btn-primary" data-toggle="modal" data-target="#editar"
                                    @click="guardarEnModel(pelicula)"><span class="fa fa-pencil-alt"></span></button>
                                <button class="btn btn-round btn-danger" data-toggle="modal" data-target="#eliminar"
                                    @click="guardarEnModel(pelicula)"><span class="fa fa-trash"></span></button>
                            </td>

                        </tr>
                    </tbody>
                </table>


            </div>
        </div>
    </div>


    <!-- Modal agregar blueray -->
    <div class="modal fade" id="nuevo" tabindex="-1" role="dialog" aria-labelledby="exampleModalLongTitle" aria-hidden="true">
        <div class="modal-dialog" role="document">
            <div class="modal-content">
                <div class="modal-header">
                    <h5 class="modal-title" id="exampleModalLongTitle">Registrar Película</h5>
                    <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                        <span aria-hidden="true">&times;</span>
                    </button>
                </div>
                <form>
                <div class="modal-body">
                    <label for="">Titulo</label>
                    <input type="text" class="form-control" placeholder="Titulo" v-model="pelicula.titulo" required>
                    <br>
                    <label for="">Clasificación</label>
                    <input type="text" class="form-control" placeholder="Clasificación" v-model="pelicula.clasificacion" required>
                    <br>
                    <label for="">País</label>
                    <input type="text" class="form-control" placeholder="País" v-model="pelicula.pais" required>
                    <br>
                    <label for="">Año</label>
                    <input type="number" class="form-control" placeholder="Año" v-model="pelicula.anio" required>
                    <br>
                    <label for="">Director</label>
                    <input type="text" class="form-control" placeholder="Director" v-model="pelicula.director" required>
                    <br>
                    <label for="">Reparto</label>
                    <input type="text" class="form-control" placeholder="Reparto" v-model="pelicula.reparto" required>
                    <br>
                    <label for="">Calificación</label>
                    <input type="number" class="form-control" placeholder="Calificación" v-model="pelicula.calificacion" required>
                    <br>
                    <label for="">Región</label>
                    <input type="text" class="form-control" placeholder="Región" v-model="pelicula.region" required>
                    <div class="alert alert-danger mt-4" v-if="errorsForm.length">
                        <ul>
                            <li v-for="e in errorsForm">{{e}}</li>
                        </ul>
                    </div>       

                </div>
                <div class="modal-footer">
                    <button type="button" class="btn btn-secondary" data-dismiss="modal">Cancelar</button>
                    <button type="button" @click="validar()" class="btn btn-primary"  >Registrar
                        Película</button>
                </div>
                </form>
            </div>
        </div>
    </div>

    <!-- Modal editar blueray -->
    <div class="modal fade" id="editar" tabindex="-1" role="dialog" aria-labelledby="exampleModalLongTitle" aria-hidden="true">
        <div class="modal-dialog" role="document">
            <div class="modal-content">
                <div class="modal-header">
                    <h5 class="modal-title" id="exampleModalLongTitle">Editar Película</h5>
                    <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                        <span aria-hidden="true">&times;</span>
                    </button>
                </div>
                <div class="modal-body">

                        <label for="">Titulo</label>
                        <input type="text" class="form-control" placeholder="Titulo" v-model="pelicula.titulo" required>
                        <br>
                        <label for="">Clasificación</label>
                        <input type="text" class="form-control" placeholder="Clasificación" v-model="pelicula.clasificacion" required>
                        <br>
                        <label for="">País</label>
                        <input type="text" class="form-control" placeholder="País" v-model="pelicula.pais" required>
                        <br>
                        <label for="">Año</label>
                        <input type="number" class="form-control" placeholder="Año" v-model="pelicula.anio" required>
                        <br>
                        <label for="">Director</label>
                        <input type="text" class="form-control" placeholder="Director" v-model="pelicula.director" required>
                        <br>
                        <label for="">Reparto</label>
                        <input type="text" class="form-control" placeholder="Reparto" v-model="pelicula.reparto" required>
                        <br>
                        <label for="">Calificación</label>
                        <input type="number" class="form-control" placeholder="Calificación" v-model="pelicula.calificacion" required>
                        <br>
                        <label for="">Región</label>
                        <input type="text" class="form-control" placeholder="Región" v-model="pelicula.region" required> 
                        <div class="alert alert-danger mt-4" v-if="errorsForm.length">
                        <ul>
                            <li v-for="e in errorsForm">{{e}}</li>
                        </ul>
                    </div>                          
                </div>
                <div class="modal-footer">
                    <button type="button" class="btn btn-secondary" data-dismiss="modal">Cancelar</button>
                    <button type="button"  @click="validarEdit()" class="btn btn-primary">Guardar
                        Cambios</button>
                </div>
            </div>
        </div>
    </div>

    <!-- Modal confirmar eliminacion -->
    <div class="modal sm" id="eliminar" tabindex="-1" role="dialog" aria-labelledby="eliminarUsuario" aria-hidden="true">
        <div class="modal-dialog" role="document">
            <div class="modal-content">
                <div class="modal-header">
                    <h5 class="modal-title" id="exampleModalLongTitle">Confirmar Eliminación</h5>
                    <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                        <span aria-hidden="true">&times;</span>
                    </button>
                </div>
                <div class="modal-body">
                    <center>¿Está seguro que desea eliminar <b>{{pelicula.titulo}}</b>? </center>
                </div>
                <div class="modal-footer">
                    <button type="button" class="btn btn-secondary" data-dismiss="modal">Cancelar</button>
                    <button type="button" class="btn btn-danger" data-dismiss="modal" @click="eliminar(pelicula.id_clave)">Eliminar</button>
                </div>
            </div>
        </div>
    </div>


</div>

<script>
    new Vue({
        el: '#app',
        data: {
            peliculas: [],
            buscador: '',
            pelicula: {
                id_clave: "",
                titulo: "",
                clasificacion: "",
                pais: "",
                anio: "",
                director: "",
                reparto: "",
                califiacion: "",
                region: "",
                disponible: ""
            },
            errorsForm: []
        },
        mounted: function () {
            this.getData();
        },
        methods: {

            getEstado(estado){
                var estado = parseInt(estado);
                if(estado == 1) return "Disponible";
                else return "Prestado";
            },

            guardarEnModel(item) {
                this.errorsForm = [];
                this.pelicula = Object.assign({}, item);
            },
            eliminar(id) {
                var url = "<?=base_url()?>index.php/api/peliculas/" + id;

                axios.delete(url)
                    .then(response => {
                        // alert('Examen borrado');
                        this.getData();
                        this.reiniciarModel();

                    }).catch(error => {
                        console.log(error);
                        this.getData();
                    });

            },
            getData() {
                var url = "<?=base_url()?>index.php/api/peliculas";
                axios.get(url).then(
                    response => {
                       this.peliculas = response.data;
                      
                        this.reiniciarModel();
                        
                      
                    }
                ).catch(
                    error => {
                        //alert('Error');
                        this.peliculas = [];
                    }
                );
            },
            registar() {
   
                var url = "<?=base_url()?>index.php/api/peliculas";
                var data = {
                    titulo: this.pelicula.titulo,
                    clasificacion: this.pelicula.clasificacion,
                    pais: this.pelicula.pais,
                    anio: this.pelicula.anio,
                    director: this.pelicula.director,
                    reparto: this.pelicula.reparto,
                    calificacion: this.pelicula.calificacion,
                    region: this.pelicula.region

                };
                axios.post(url, data).then(
                    response => {
                        this.getData();
                        this.reiniciarModel();
                    }
                ).catch(
                    error => {
                        alert('Error');

                    }
                );
            },
            guardarCambios() {
                var url = "<?=base_url()?>index.php/api/peliculas/" + this.pelicula.id_clave;
                var data = {
                    titulo: this.pelicula.titulo,
                    clasificacion: this.pelicula.clasificacion,
                    pais: this.pelicula.pais,
                    anio: this.pelicula.anio,
                    director: this.pelicula.director,
                    reparto: this.pelicula.reparto,
                    calificacion: this.pelicula.calificacion,
                    region: this.pelicula.region
                };
                axios.put(url, data).then(
                    response => {
                        this.getData();
                        this.reiniciarModel();
                    }
                ).catch(
                    error => {
                        console.log(error);
                        alert('Error');
                    }
                );
            },
            reiniciarModel() {
                this.pelicula = {
                /*   id_clave: "",*/
                    titulo: "",
                    clasificacion: "",
                    pais: "",
                    anio: "",
                    director: "",
                    reparto: "",
                    califiacion: "",
                    region: ""
                };
            },

            validar(){
                
                ok = true;
                this.errorsForm = [];

                if(!this.pelicula.titulo){
                    this.errorsForm.push("Título requerido");
                    ok = false;
                }
                if(!this.pelicula.clasificacion){
                    this.errorsForm.push("Clasificación requerido");
                    ok = false;
                }
                if(this.pelicula.calificacion>10){
                    this.errorsForm.push("La calificación máxima es 10");
                    ok = false;
                }


                if(ok){
                    this.registar();
                    $('#nuevo').modal("hide");
                }
            },

             validarEdit(){
                
                ok = true;
                this.errorsForm = [];

               if(!this.pelicula.titulo){
                    this.errorsForm.push("Título requerido");
                    ok = false;
                }
                if(!this.pelicula.clasificacion){
                    this.errorsForm.push("Clasificación requerido");
                    ok = false;
                }
                if(this.pelicula.calificacion>10){
                    this.errorsForm.push("La calificación máxima es 10");
                    ok = false;
                }

                if(ok){
                    this.guardarCambios();
                    $('#editar').modal('hide');
                }
            }
        },
        computed: {
            filtro: function () {
                if (this.buscador === '') {
                    return this.peliculas;
                } else {
                    return this.peliculas.filter((item) => {
                        return (item.titulo.toString()  + item.clasificacion
                            .toString()).toLowerCase().includes(this.buscador
                            .toLowerCase());
                    });
                }
            }

        }



    });

    /*  $(document).ready(function(){
         $('#tabla-usuarios').DataTable();
     });
       */
</script>