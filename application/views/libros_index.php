<div id="app" class="row" style="margin-top: 25px;">
    <div class="col-12">
        <div class="card">
            <div class="card-header bg-light">
                <h1>Libros</h1>
            </div>
            <div class="card-body">

                <div class="row" style="margin-bottom: 15px;">
                    <div class="col-5">
                    </div>
                    <div class="col-3">
                        <input type="text" class="form-control" v-model='buscador' placeholder="Buscar Libro..." aria-desc ribedby="basic-addon1">
                    </div>
                    <div class="col-4">
                        <button class="btn btn-success" data-toggle="modal" data-target="#nuevo" @click="reiniciarModel">Registrar
                            Libro
                        </button>
                    </div>
                </div>

                <table class="table table-striped col-12">
                    <thead>

                        <th>Título</th>
                        <th>Autor</th>
                        <th>Clasificación</th>
                        <th>Pie de Imprenta</th>
                        <th>Páginas</th>
                        <th>ISBN</th>
                        <th>No. Adquisición</th>
                        <th>Temas</th>
                        <th>Estado</th>
                        <th>Acciones</th>

                    </thead>
                    <tbody>
                        <tr v-for="cd in filtro">

                            <td>{{cd.titulo}}</td>
                            <td>{{cd.autor}}</td>
                            <td>{{cd.clasificacion}}</td>
                            <td>{{cd.pie_imprenta}}</td>
                            <td>{{cd.paginas}}</td>
                            <td>{{cd.isbn}}</td>
                            <td>{{cd.no_adquisicion}}</td>
                            <td>{{cd.temas}}</td>
                            <td>{{getEstado(cd.disponible)}}</td>  

                            <td>
                                <button class="btn btn-round btn-primary" data-toggle="modal" data-target="#editar" @click="guardarEnModel(cd)"><span
                                        class="fa fa-pencil-alt"></span></button>
                                <button class="btn btn-round btn-danger" data-toggle="modal" data-target="#eliminar" @click="guardarEnModel(cd)"><span
                                        class="fa fa-trash"></span></button>
                            </td>

                        </tr>
                    </tbody>
                </table>


            </div>
        </div>
    </div>


    <!-- Modal agregar libro -->
    <div class="modal fade" id="nuevo" tabindex="-1" role="dialog" aria-labelledby="exampleModalLongTitle" aria-hidden="true">
        <div class="modal-dialog" role="document">
            <div class="modal-content">
                <div class="modal-header">
                    <h5 class="modal-title" id="exampleModalLongTitle">Registrar Libro</h5>
                    <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                        <span aria-hidden="true">&times;</span>
                    </button>
                </div>
                <form id="form_validation"  @submit.prevent="validar()" >
                <div class="modal-body">
                    <label for="">Titulo</label>
                    <input type="text" name="titulo" class="form-control" placeholder="Titulo" v-model="libro.titulo">
                    <br>
                    <label for="">Autor</label>
                    <input type="text" name="autor" class="form-control" placeholder="Autor" v-model="libro.autor" >
                    <br>
                    <label for="">Clasificación</label>
                    <input type="text" name="clasificacion" class="form-control" placeholder="Clasificación" v-model="libro.clasificacion" >
                    <br>
                    <label for="">Pie de Imprenta</label>
                    <input type="text" name="pie_imprenta" class="form-control" placeholder="Pie de Imprenta" v-model="libro.pie_imprenta" >
                    <br>
                    <label for="">Páginas</label>
                    <input type="number" name="paginas" class="form-control" placeholder="Páginas" v-model="libro.paginas" >
                    <br>
                    <label for="">ISBN</label>
                    <input type="text" name="isbn" class="form-control" placeholder="ISBN" v-model="libro.isbn" >
                    <br>
                    <label for="">No. Adquisición</label>
                    <input type="text" name="NoAdqusicion" class="form-control" placeholder="No. Adquisición" v-model="libro.no_adquisicion" >
                    <br>
                    <label for="">Temas</label>
                    <input type="text" name="temas" class="form-control" placeholder="Temas" v-model="libro.temas" >
                    <br>

                </div>
                <div class="modal-footer">
                    <button type="button" class="btn btn-secondary" data-dismiss="modal">Cancelar</button>
                    <button id="validar" type="submit" class="btn btn-primary">Registrar
                        Libro
                    </button>
                </form>
                </div>
            </div>
        </div>
    </div>

    <!-- Modal editar libro -->
    <div class="modal fade" id="editar" tabindex="-1" role="dialog" aria-labelledby="exampleModalLongTitle" aria-hidden="true">
        <div class="modal-dialog" role="document">
            <div class="modal-content">
                <div class="modal-header">
                    <h5 class="modal-title" id="exampleModalLongTitle">Editar Libro</h5>
                    <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                        <span aria-hidden="true">&times;</span>
                    </button>
                </div>
                <form id="form_validation"  @submit.prevent="validar()" >
                <div class="modal-body">

                    <label for="">Titulo</label>
                    <input type="text" name="titulo" class="form-control" placeholder="Titulo" v-model="libro.titulo" required>
                    <br>
                    <label for="">Autor</label>
                    <input type="text" name="autor" class="form-control" placeholder="Autor" v-model="libro.autor" required>
                    <br>
                    <label for="">Clasificación</label>
                    <input type="text" name="clasificacion" class="form-control" placeholder="Clasificación" v-model="libro.clasificacion" required>
                    <br>
                    <label for="">Pie de Imprenta</label>
                    <input type="text" class="form-control" placeholder="Pie de Imprenta" v-model="libro.pie_imprenta" required>
                    <br>
                    <label for="">Páginas</label>
                    <input type="number" class="form-control" placeholder="Páginas" v-model="libro.paginas" required>
                    <br>
                    <label for="">ISBN</label>
                    <input type="text" class="form-control" placeholder="ISBN" v-model="libro.isbn" required>
                    <br>
                    <label for="">No. Adquisición</label>
                    <input type="text" class="form-control" placeholder="No. Adquisición" v-model="libro.no_adquisicion" required>
                    <br>
                    <label for="">Temas</label>
                    <input type="text" class="form-control" placeholder="Temas" v-model="libro.temas" required>
                    <br>

                </div>
                <div class="modal-footer">
                    <button type="button" class="btn btn-secondary" data-dismiss="modal">Cancelar</button>
                    <button id="validar" type="submit" class="btn btn-primary" data-dismiss="modal" v-on:click="guardarCambios">Guardar
                        Cambios
                    </button>
                    </form>
                </div>
            </div>
        </div>
    </div>

    <!-- Modal confirmar eliminacion -->
    <div class="modal sm" id="eliminar" tabindex="-1" role="dialog" aria-labelledby="eliminarUsuario" aria-hidden="true">
        <div class="modal-dialog" role="document">
            <div class="modal-content">
                <div class="modal-header">
                    <h5 class="modal-title" id="exampleModalLongTitle">Confirmar Eliminación</h5>
                    <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                        <span aria-hidden="true">&times;</span>
                    </button>
                </div>
                <div class="modal-body">
                    <center>¿Está seguro que desea eliminar <b>{{libro.titulo}}</b>? </center>
                </div>
                <div class="modal-footer">
                    <button type="button" class="btn btn-secondary" data-dismiss="modal">Cancelar</button>
                    <button type="button" class="btn btn-danger" data-dismiss="modal" @click="eliminar(libro.id_clave)">Eliminar</button>
                </div>
            </div>
        </div>
    </div>
</div>

<script>
    new Vue({
        el: '#app',
        data: {
            libros: [],
            buscador: '',
            libro: {
                id_clave: "",
                titulo: "",
                autor: "",
                clasificacion: "",
                pie_imprenta: "",
                paginas: "",
                isbn: "",
                no_adquisicion: "",
                temas: "",
                disponible: ""
            }
        },
        mounted: function () {
            this.getData();
        },
        methods: {

            getEstado(estado){
                var estado = parseInt(estado);
                if(estado == 1) return "Disponible";
                else return "Prestado";
            },

            guardarEnModel(item) {
                this.errorsForm = [];
                this.libro = Object.assign({}, item);
            },
            eliminar(id) {
                var url = "<?=base_url()?>index.php/api/libros/" + id;

                axios.delete(url)
                    .then(response => {
                        // alert('Examen borrado');
                        this.getData();
                        this.reiniciarModel();

                    }).catch(error => {
                        console.log(error);
                        this.getData();
                    });

            },
            getData() {
                var url = "<?=base_url()?>index.php/api/libros";
                axios.get(url).then(
                    response => {
                       this.libros = response.data;
                      
                        this.reiniciarModel();
                        
                      
                    }
                ).catch(
                    error => {
                        //alert('Error');
                        this.libros = [];
                    }
                );
            },
            registarLibro() {
   
                var url = "<?=base_url()?>index.php/api/libros";
                var data = {
                    titulo: this.libro.titulo,
                    autor: this.libro.autor,
                    clasificacion: this.libro.clasificacion,
                    pie_imprenta: this.libro.pie_imprenta,
                    paginas: this.libro.paginas,
                    isbn: this.libro.isbn,
                    no_adquisicion: this.libro.no_adquisicion,
                    temas: this.libro.temas

                };
                axios.post(url, data).then(
                    response => {
                        this.getData();
                        this.reiniciarModel();
                    }
                ).catch(
                    error => {
                        alert('Error');

                    }
                );
            },
            guardarCambios() {
                var url = "<?=base_url()?>index.php/api/libros/" + this.libro.id_clave;
                var data = {
                    titulo: this.libro.titulo,
                    autor: this.libro.autor,
                    clasificacion: this.libro.clasificacion,
                    pie_imprenta: this.libro.pie_imprenta,
                    paginas: this.libro.paginas,
                    isbn: this.libro.isbn,
                    no_adquisicion: this.libro.no_adquisicion,
                    temas: this.libro.temas
                };
                axios.put(url, data).then(
                    response => {
                        this.getData();
                        this.reiniciarModel();
                    }
                ).catch(
                    error => {
                        console.log(error);
                        alert('Error');
                    }
                );
            },
            reiniciarModel() {
                this.libro = {
                    id_clave: "",
                    titulo: "",
                    autor: "",
                    clasificacion: "",
                    pie_imprenta: "",
                    paginas: "",
                    isbn: "",
                    no_adquisicion: "",
                    temas: "",
                    disponible: ""
                };
            },
            validar(){
                this.registarLibro();
                    $('#nuevo').modal("hide");

            }
        },
        computed: {
            filtro: function () {
                if (this.buscador === '') {
                    return this.libros;
                } else {
                    return this.libros.filter((item) => {
                        return (item.titulo.toString()  + item.clasificacion
                            .toString()).toLowerCase().includes(this.buscador
                            .toLowerCase());
                    });
                }
            }

        }



    });

    /*  $(document).ready(function(){
         $('#tabla-usuarios').DataTable();
     });
       */
</script>

