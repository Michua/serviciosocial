<?php $this->load->view('header-admin');?>
<div id="app">
    <div class="col-md-12" style="padding-top: 4%">
        <div class="col-md-12 panel">
        <div class="col-md-12 panel-heading">
            <h4>Editar Clasificación de <?=$titulo?><span class="<?=$clase?>" style="padding-left: 4px"></span></h4>
        </div>
       
        <div class="col-md-12 panel-body" style="padding-bottom:30px;">
            <div class="col-md-12">
            <form class="cmxform" id="signupForm" action="" v-on:submit.prevent="actualizar">
                <div class="col-md-12">
                    <div class="form-group form-animate-text" style="margin-top:40px !important;">
                        <input type="text" class="form-text" id="nombre" name="nombre" v-model="modeloNombre" required>
                        <span class="bar"></span>
                        <label>Nombre</label>
                    </div>
                </div>
                <div class="col-md-12">
                    <input type="submit" class="btn btn-round btn-success" value="Guardar cambios">
                    <a class=" btn btn-round btn-primary" href="<?=base_url()?>index.php/clasificaciones/<?=$funcion?>">Cancelar</a> 
                </div>
            </form>
        </div>
        </div>
    </div>
    </div>
</div>


<script>
    new Vue({
        el: '#app',
        data: {
            modeloNombre: '<?=$nombre?>'
        },
        methods:{
            actualizar(){
                if(this.modeloNombre !== ''){
                    var url = "<?=base_url()?>index.php/api/clasificacion/<?=$id?>";
                    axios.put(url,{
                        nombre: this.modeloNombre 
                    })
                    .then( response =>{
                        //alert("Exito");
                        
                    }).catch( error =>{

                    });
                    window.location.replace('<?=base_url()?>index.php/clasificaciones/<?=$funcion?>');
                }else{
                    alert('Campo vacío')
                }
            }
        }
    });
</script>
<?php $this->load->view('footer-admin');?>

