<div id="app" class="row" style="margin-top: 25px;">
    <div class="col-12">
        <div class="card">
            <div class="card-header bg-light">
                <h1>prestamo</h1>
            </div>
            <div class="card-body">

                <div class="row" style="margin-bottom: 15px;">
                    <div class="col-5">
                    </div>
                    <div class="col-3">
                        <input type="text" class="form-control" v-model='buscador' placeholder="Buscar prestamo..."
                            aria-desc ribedby="basic-addon1">
                    </div>
                    <div class="col-4">
                        <a class="btn btn-success" href="<?=base_url()?>index.php/prestamos/nuevo"
                            @click="reiniciarModel">Registrar
                            Prestamo</a>
                    </div>
                </div>

                <table class="table table-striped col-12">
                    <thead>
                        <th>Título</th>
                        <th>Prestador</th>
                        <th>Apellido paterno</th>
                        <th>Apellido materno</th>
                        <th>Usuario</th>
                        <th>Apellido paterno</th>
                        <th>Apellido materno</th>
                        <th>Fecha prestamo</th>
                        <th>Fecha prometida</th>

                    </thead>
                    <tbody>
                        <tr v-for="p in prestamo">
                            <td>{{p.titulo}}</td>
                            <td>{{p.usuario.nombre}}</td>
                            <td>{{p.usuario.ape_pat}}</td>
                            <td>{{p.usuario.ape_mat}}</td>
                            <td>{{p.prestador.nombre}}</td>
                            <td>{{p.prestador.ape_pat}}</td>
                            <td>{{p.prestador.ape_mat}}</td>
                            <td>{{p.fecha_prestamo}}</td>
                            <td>{{p.fecha_prometida}}</td>
                            <td>
                                <button class="btn btn-round btn-primary" data-toggle="modal" data-target="#editar"
                                    @click="guardarEnModel(prestamo)"><span class="fa fa-pencil-alt"></span></button>
                                
                            </td>

                        </tr>
                    </tbody>
                </table>


            </div>
        </div>
    </div>


    <!-- Modal agregar libro -->
    <div class="modal fade" id="nuevo" tabindex="-1" role="dialog" aria-labelledby="exampleModalLongTitle"
        aria-hidden="true">
        <div class="modal-dialog" role="document">
            <div class="modal-content">
                <div class="modal-header">
                    <h5 class="modal-title" id="exampleModalLongTitle">Registrar prestamo</h5>
                    <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                        <span aria-hidden="true">&times;</span>
                    </button>
                </div>
                <div class="modal-body">
                    <label for="">Titulo</label>
                    <input type="text" class="form-control" placeholder="Titulo" v-model="prestamo.titulo" required>
                    <br>
                    <label for="">nombre_prest</label>
                    <input type="text" class="form-control" placeholder="Autor" v-model="prestamo.nombre" required>
                    <br>
                    <label for="">Clasificación</label>
                    <input type="text" class="form-control" placeholder="Clasificación" v-model="prestamo.ape_pat"
                        required>
                    <label for="">Fecha prestamo</label>
                    <input type="date" class="form-control" placeholder="fecha_prestamo"
                        v-model="prestamo.fecha_prestamo" required>

                    <label for="">Fecha devolucion</label>
                    <input type="date" class="form-control" placeholder="fecha_prestamo"
                        v-model="prestamo.fecha_prestamo" required>
                </div>
                <div class="modal-footer">
                    <button type="button" class="btn btn-secondary" data-dismiss="modal">Cancelar</button>
                    <button type="button" class="btn btn-primary" data-dismiss="modal"
                        v-on:click="registrarPrestamo">Registrar
                        Prestamo</button>
                </div>
            </div>
        </div>
    </div>

    <!-- Modal editar libro -->
    <div class="modal fade" id="editar" tabindex="-1" role="dialog" aria-labelledby="exampleModalLongTitle"
        aria-hidden="true">
        <div class="modal-dialog" role="document">
            <div class="modal-content">
                <div class="modal-header">
                    <h5 class="modal-title" id="exampleModalLongTitle">Editar disco</h5>
                    <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                        <span aria-hidden="true">&times;</span>
                    </button>
                </div>
                <div class="modal-body">

                    <label for="">Titulo</label>
                    <input type="text" class="form-control" placeholder="Titulo" v-model="prestamo.titulo" required>
                    <br>
                    <label for="">nombre_prest</label>
                    <input type="text" class="form-control" placeholder="Autor" v-model="prestamo.nombre" required>
                    <br>
                    <label for="">Clasificación</label>
                    <input type="text" class="form-control" placeholder="Clasificación"
                        v-model="prestamo.ape_pat_prest_prestamo" required>
                </div>
                <div class="modal-footer">
                    <button type="button" class="btn btn-secondary" data-dismiss="modal">Cancelar</button>
                    <button type="button" class="btn btn-primary" data-dismiss="modal"
                        v-on:click="guardarCambios">Guardar
                        Cambios</button>
                </div>
            </div>
        </div>
    </div>

    <!-- Modal confirmar eliminacion -->
    <div class="modal sm" id="eliminar" tabindex="-1" role="dialog" aria-labelledby="eliminarUsuario"
        aria-hidden="true">
        <div class="modal-dialog" role="document">
            <div class="modal-content">
                <div class="modal-header">
                    <h5 class="modal-title" id="exampleModalLongTitle">Confirmar Eliminación</h5>
                    <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                        <span aria-hidden="true">&times;</span>
                    </button>
                </div>
                <div class="modal-body">
                    <center>¿Está seguro que desea eliminar <b>{{prestamo.titulo}}</b>? </center>
                </div>
                <div class="modal-footer">
                    <button type="button" class="btn btn-secondary" data-dismiss="modal">Cancelar</button>
                    <button type="button" class="btn btn-danger" data-dismiss="modal"
                        @click="eliminarLibro(prestamo.id)">Eliminar</button>
                </div>
            </div>
        </div>
    </div>


</div>

<script>
    new Vue({
        el: '#app',
        data: {
            discos: [],
            buscador: '',
            prestamo: [],
            prestamoGuardar: {
                id_clave: "",
                fecha_prometida: "",
                id_prestador: "",
                id_usuario: ""
            }
        },
        mounted: function () {
            this.getData();
        },
        methods: {
            guardarEnModel(item) {
                this.prestamo = Object.assign({}, item);
            },
            eliminar(id) {
                var url = "<?=base_url()?>index.php/api/prestamo/" + id;

                axios.delete(url)
                    .then(response => {
                        // alert('Examen borrado');
                        this.getData();
                        this.reiniciarModel();

                    }).catch(error => {
                        console.log(error);
                        this.getData();
                    });

            },
            getData() {
                var url = "<?=base_url()?>index.php/api/prestamosPor/3";
                axios.get(url).then(
                    response => {
                        this.prestamo = response.data;

                    }
                ).catch(
                    error => {
                        alert("Ocurrio un error al consultar los prestamos")
                    }
                );
            },
            registrarPrestamo() {

                var url = "<?=base_url()?>index.php/api/prestamo";
                var data = {
                    titulo: this.prestamo.titulo,
                    nombre_prest: this.prestamo.nombre_prest,
                    ape_pat_prest_prestamo: this.prestamo.ape_pat_prest_prestamo
                };
                axios.post(url, data).then(
                    response => {
                        this.getData();
                        this.reiniciarModel();
                    }
                ).catch(
                    error => {
                        alert('Error');

                    }
                );
            },
            guardarCambios() {
                var url = "<?=base_url()?>index.php/api/prestamo/" + this.prestamo.id;
                var data = {
                    titulo: this.prestamo.titulo,
                    nombre_prest: this.prestamo.nombre_prest,
                    ape_pat: this.prestamo.ape_pat
                };
                axios.put(url, data).then(
                    response => {
                        this.getData();
                        this.reiniciarModel();
                    }
                ).catch(
                    error => {
                        console.log(error);
                        alert('Error');
                    }
                );
            },
            reiniciarModel() {
                this.prestamo = {
                    titulo: "",
                    nombre_prest: "",
                    ape_pat_prest: ""
                };
            }
        },
        computed: {
            filtro: function () {
                if (this.buscador === '') {
                    return this.discos;
                } else {
                    return this.discos.filter((item) => {
                        return (item.titulo.toString() + item.nombre_prest.toString() + item.ape_pat_prest_prestamo
                            .toString()).toLowerCase().includes(this.buscador
                                .toLowerCase());
                    });
                }
            }

        }



    });

    /*  $(document).ready(function(){
         $('#tabla-usuarios').DataTable();
     });
       */
</script>