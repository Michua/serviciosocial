<div id="app" class="row" style="margin-top: 25px;">
    <div class="col-12">
        <div class="card">
            <div class="card-header bg-light">
                <h1>Prestadores</h1>
            </div>
            <div class="card-body">

                <div class="row" style="margin-bottom: 15px;">
                    <div class="col-5">
                    </div>
                    <div class="col-3">
                        <input type="text" class="form-control" v-model='buscadorPrestador' placeholder="Buscar Prestador..."
                            aria-desc ribedby="basic-addon1">
                    </div>
                    <div class="col-4">
                        <button class="btn btn-success" data-toggle="modal" data-target="#nuevo" @click="reiniciarModel">Registrar
                        Prestador</button>
                    </div>
                </div>

                <table class="table table-striped col-12">
                <thead>
                        <th>Clave</th>
                        <th>Nombre</th>
                        <th>Apellido Paterno</th>
                        <th>Apellido Materno</th>
                        <th>Correo</th>
                        <th>Teléfono</th>

                        
                    </thead>
                    <tbody>
                        <tr v-for="prestador in filtrarPrestador">
                            <td>{{prestador.id_prestador}}</td>
                            <td>{{prestador.nombre}}</td>
                            <td>{{prestador.ape_pat}}</td>
                            <td>{{prestador.ape_mat}}</td>
                            <td>{{prestador.correo}}</td>
                            <td>{{prestador.telefono}}</td>

                            <td>
                                <button class="btn btn-round btn-primary" data-toggle="modal" data-target="#editar"
                                    @click="editarPrestador(prestador)"><span class="fa fa-pencil-alt"></span></button>
                                <button class="btn btn-round btn-danger" data-toggle="modal" data-target="#eliminar"
                                    @click="editarPrestador(prestador)"><span class="fa fa-trash"></span></button>
                            </td>

                        </tr>
                    </tbody>
                </table>


            </div>
        </div>
    </div>


    <!-- Modal agregar Prestador -->
    <div class="modal fade" id="nuevo" tabindex="-1" role="dialog" aria-labelledby="exampleModalLongTitle"
        aria-hidden="true">
        <div class="modal-dialog" role="document">
            <div class="modal-content">
                <div class="modal-header">
                    <h5 class="modal-title" id="exampleModalLongTitle">Registrar prestador</h5>
                    <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                        <span aria-hidden="true">&times;</span>
                    </button>
                </div>
                <form> 
                <div class="modal-body">

                    <label for="">Clave</label>
                    <input type="number" class="form-control" placeholder="Clave" v-model="modelPrestador.id_prestador" required>
                    <br>
                    <label for="">Nombre</label>
                    <input type="text" class="form-control" placeholder="Nombre" v-model="modelPrestador.nombre" required>
                    <br>
                    <label for="">Apellido Paterno</label>
                    <input type="text" class="form-control" placeholder="Apellido Paterno" v-model="modelPrestador.ape_pat"
                        required>
                    <br>
                    <label for="">Apellido Materno</label>
                    <input type="text" class="form-control" placeholder="Apellido Materno" v-model="modelPrestador.ape_mat"
                        required>
                    <br>
                    <label for="">Correo</label>
                    <input type="text" class="form-control" placeholder="Correo" v-model="modelPrestador.correo"
                        required>
                    <br>
                    <label for="">Teléfono</label>
                    <input type="number" class="form-control" placeholder="Teléfono" v-model="modelPrestador.telefono" required>
                    <br>
                    <label for="">Contraseña</label>
                    <input type="text" class="form-control" placeholder="Contraseña" v-model="modelPrestador.contrasena" required>
                    <div class="alert alert-danger mt-4" v-if="errorsForm.length">
                        <ul>
                            <li v-for="e in errorsForm">{{e}}</li>
                        </ul>
                    </div>
                </div>
                <div class="modal-footer">
                    <button type="button" class="btn btn-secondary" data-dismiss="modal">Cancelar</button>
                    <button type="button"  @click="validar()" class="btn btn-primary" >Registrar
                        Prestador</button>
                </div>
                </form>
            </div>
        </div>
    </div>

    <!-- Modal editar Prestador -->
    <div class="modal fade" id="editar" tabindex="-1" role="dialog" aria-labelledby="exampleModalLongTitle" aria-hidden="true">
        <div class="modal-dialog" role="document">
            <div class="modal-content">
                <div class="modal-header">
                    <h5 class="modal-title" id="exampleModalLongTitle">Editar Prestador</h5>
                    <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                        <span aria-hidden="true">&times;</span>
                    </button>
                </div>
                <div class="modal-body">
                    <label for="">Nombre</label>
                    <input type="text" class="form-control" placeholder="Nombre" v-model="modelPrestador.nombre" required>
                    <br>
                    <label for="">Apellido Paterno</label>
                    <input type="text" class="form-control" placeholder="Apellido Paterno" v-model="modelPrestador.ape_pat" required>
                    <br>
                    <label for="">Apellido Materno</label>
                    <input type="text" class="form-control" placeholder="Apellido Materno" v-model="modelPrestador.ape_mat"  required>
                    <br>                   
                    <label for="">Correo</label>
                    <input type="text" class="form-control" placeholder="Correo" v-model="modelPrestador.correo" required>
                    <br>                 
                    <label for="">Teléfono</label>
                    <input type="number" class="form-control" placeholder="Teléfono" v-model="modelPrestador.telefono" required>
                    <br>                
                    <label for="">Contraseña</label>
                    <input type="text" class="form-control" placeholder="Contraseña" v-model="modelPrestador.contrasena" required>
                    <div class="alert alert-danger mt-4" v-if="errorsForm.length">
                        <ul>
                            <li v-for="e in errorsForm">{{e}}</li>
                        </ul>
                    </div>
                </div>
                <div class="modal-footer">
                    <button type="button" class="btn btn-secondary" data-dismiss="modal">Cancelar</button>
                    <button type="button" @click="validarEdit()" class="btn btn-primary" >Guardar
                        Cambios</button>
                </div>
            </div>
        </div>
    </div>

    <!-- Modal confirmar eliminacion Prestador-->
    <div class="modal sm" id="eliminar" tabindex="-1" role="dialog" aria-labelledby="eliminarPrestador" aria-hidden="true">
        <div class="modal-dialog" role="document">
            <div class="modal-content">
                <div class="modal-header">
                    <h5 class="modal-title" id="exampleModalLongTitle">Confirmar Eliminación</h5>
                    <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                        <span aria-hidden="true">&times;</span>
                    </button>
                </div>
                <div class="modal-body">
                    <center>¿Está seguro que desea eliminar <b>{{modelPrestador.nombre}}</b>? </center>
                </div>
                <div class="modal-footer">
                    <button type="button" class="btn btn-secondary" data-dismiss="modal">Cancelar</button>
                    <button type="button" class="btn btn-danger" data-dismiss="modal" @click="eliminarPrestador(modelPrestador.id_prestador)">Eliminar</button>
                </div>
            </div>
        </div>
    </div>


</div>

<script>
    new Vue({
        el: '#app',
        data: {
            prestador: [],
            buscadorPrestador: '',
            saludo: 'hola :)',
            modelPrestador: {
                id_prestador: "",
                nombre: "",
                ape_pat: "",
                ape_mat:"",
                correo: "",
                telefono: "",
                contrasena: ""
            },
            errorsForm: []
        },
        mounted: function () {
            this.getPrestador();
        },
        methods: {
            editarPrestador(prestador) {
                this.errorsForm = [];
                this.modelPrestador = Object.assign({}, prestador);
            },
            eliminarPrestador(id) {
                var url = "<?=base_url()?>index.php/api/prestadores/" + id;

                axios.delete(url)
                    .then(response => {
                        // alert('Examen borrado');
                        this.getPrestador();
                        this.reiniciarModel();

                    }).catch(error => {
                        console.log(error);
                        this.getPrestador();
                    });

            },
            getPrestador() {
                var url = "<?=base_url()?>index.php/api/prestadores";
                axios.get(url).then(
                    response => {
                        this.prestador = response.data;
                        this.reiniciarModel();

                    }
                ).catch(
                    error => {
                        //alert('Error');
                        this.prestador = [];
                    }
                );
            },
            registarPrestador() {
                console.log(this.modelPrestador);
                var url = "<?=base_url()?>index.php/api/prestadores";
                var data = {
                    id_prestador: this.modelPrestador.id_prestador,
                    nombre: this.modelPrestador.nombre,
                    ape_pat: this.modelPrestador.ape_pat,
                    ape_mat: this.modelPrestador.ape_mat,
                    correo: this.modelPrestador.correo,
                    telefono: this.modelPrestador.telefono,
                    contrasena: this.modelPrestador.contrasena
                };
                axios.post(url, data).then(
                    response => {
                        this.getPrestador();
                        this.reiniciarModel();
                    }
                ).catch(
                    error => {
                        console.log(error);
                        alert('Error');

                    }
                );
            },
            guardarCambios() {
                var url = "<?=base_url()?>index.php/api/prestadores/" + this.modelPrestador.id_prestador;
                var data = {
                    nombre: this.modelPrestador.nombre,
                    ape_pat: this.modelPrestador.ape_pat,
                    ape_mat: this.modelPrestador.ape_mat,
                    correo: this.modelPrestador.correo,
                    telefono: this.modelPrestador.telefono,
                    contrasena: this.modelPrestador.contrasena

                };
                axios.put(url, data).then(
                    response => {
                        this.getPrestador();
                        this.reiniciarModel();
                    }
                ).catch(
                    error => {
                        console.log(error);
                        alert('Error');
                    }
                );
            },
            reiniciarModel() {
                this.modelPrestador = {
                    nombre: "",
                    ape_pat: "",
                    ape_mat: "",
                    correo: "",
                    telefono: "",
                    contrasena: ""
                }
            },

            validar(){
                
                ok = true;
                this.errorsForm = [];

                if(!this.modelPrestador.id_prestador){
                    this.errorsForm.push("Clave requerida");
                    ok = false;
                }
                if(!this.modelPrestador.nombre){
                    this.errorsForm.push("Nombre requerido");
                    ok = false;
                }
                if(!this.modelPrestador.ape_pat){
                    this.errorsForm.push("Apellido Paterno requerido");
                    ok = false;
                }
                if(!this.modelPrestador.ape_mat){
                    this.errorsForm.push("Apellido Materno requerido");
                    ok = false;
                }
                if(!this.modelPrestador.correo){
                    this.errorsForm.push("Correo requerido");
                    ok = false;
                }
                if(!this.modelPrestador.telefono){
                    this.errorsForm.push("Teléfono requerido");
                    ok = false;
                }
                if(!this.modelPrestador.contrasena){
                    this.errorsForm.push("Contraseña requerido");
                    ok = false;
                }
                if(this.modelPrestador.telefono.length>10){
                    this.errorsForm.push("Máximo 10 dígitos en teléfono");
                    ok = false; 
                }


                if(ok){
                    this.registarPrestador();
                    $('#nuevo').modal('hide');
                }
            },

            validarEdit(){
                ok = true;
                this.errorsForm = [];

                               if(!this.modelPrestador.id_prestador){
                    this.errorsForm.push("Clave requerida");
                    ok = false;
                }
                if(!this.modelPrestador.nombre){
                    this.errorsForm.push("Nombre requerido");
                    ok = false;
                }
                if(!this.modelPrestador.ape_pat){
                    this.errorsForm.push("Apellido Paterno requerido");
                    ok = false;
                }
                if(!this.modelPrestador.ape_mat){
                    this.errorsForm.push("Apellido Materno requerido");
                    ok = false;
                }
                if(!this.modelPrestador.correo){
                    this.errorsForm.push("Correo requerido");
                    ok = false;
                }
                if(!this.modelPrestador.telefono){
                    this.errorsForm.push("Teléfono requerido");
                    ok = false;
                }
                if(!this.modelPrestador.contrasena){
                    this.errorsForm.push("Contraseña requerido");
                    ok = false;
                }
                if(this.modelPrestador.telefono.length>10){
                    this.errorsForm.push("Máximo 10 dígitos en teléfono");
                    ok = false; 
                }


                if(ok){
                    this.guardarCambios();
                    $('#editar').modal('hide');
                }
            }
        },
        computed: {
            filtrarPrestador: function () {
                if (this.buscadorPrestador === '') {
                    return this.prestador;
                } else {
                    return this.prestador.filter((prestador) => {
                        return (prestador.nombre.toString() + prestador.ape_pat.toString() + prestador.ape_mat
                            .toString()).toLowerCase().includes(this.buscadorPrestador
                            .toLowerCase());
                    });
                }
            }

        }



    });

    /*  $(document).ready(function(){
         $('#tabla-usuarios').DataTable();
     });
       */
</script>