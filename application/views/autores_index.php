<? $this->load->view('header-admin')?>
	
	
<div id="app">
     <div class="col-md-12 top-20 padding-0">
             <div class="col-md-12">
               <div class="panel">
                 <div class="panel-heading">
                     <h3><?=$titulo?> <span class="<?=$clase?>"></span></h3>     
                     <div class="col-md-offset-7 col-md-2">
                         <a class="btn btn-round btn-success" href="<?= site_url('clasificaciones/nuevo');?><?=$tipo?>"> <i class="fa fa-folder"></i><span style="padding-left: 4px">Agregar</span></a>    
                     </div>   
                     <div class="input-group col-md-3 ">
                         <span class="input-group-addon" id="basic-addon1"><i class="fa fa-search"></i></span>
                         <input type="text" class="form-control" v-model='buscador'placeholder="Buscar clasificación..." aria-desc ribedby="basic-addon1">
                     </div>
                     
                 </div>
                 <div class="panel-body">
                   <div class="responsive-table">
                   <table id="tabla-usuarios" class="table table-striped table-bordered" width="100%" cellspacing="0">
                   <thead>
                     <tr>
                       <th>Nombre</th>
                       <th>Acciones</th>
                     </tr>
                   </thead>
                   <tbody>
                     <tr v-for="autor in autores">
                         <td>{{autor.nombre}}</td>
                         <td>
                             <a :href="'<?=base_url()?>index.php/clasificaciones/editar/'+autor.id" class="btn btn-round btn-primary"><span class="fa fa-pencil"></span></a>
                             <button class="btn btn-round btn-danger" @click="eliminar(autor.id)"><span class="fa fa-trash"></span></button>
                         </td>
                     </tr>
                   </tbody>
                     </table>
                   </div>
               </div>
             </div>
           </div>  
           </div>
</div>

<script>
	 new Vue({
        el: '#app',
        data: {
            autores: null,
            buscador: ''
        },
        mounted: function(){
            this.getAutores();
        },
        methods: {
        	getAutores(){
        		var url = "<?=base_url()?>index.php/api/autores/<?=$tipo?>";
	            axios.get(url).then(
	                response=>{
	                    this.autores = response.data;
	                }
	            ).catch(
	                error => {
	                  //  alert('Error');
	                }
	            );
        	}
        }
    });
</script>
	

	
	
<? $this->load->view('footer-admin')?>