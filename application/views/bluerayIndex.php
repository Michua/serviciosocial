<div id="app" class="row" style="margin-top: 25px;">
    <div class="col-12">
        <div class="card">
            <div class="card-header bg-light">
                <h1>Blueray</h1>
            </div>
            <div class="card-body">

                <div class="row" style="margin-bottom: 15px;">
                    <div class="col-5">
                    </div>
                    <div class="col-3">
                        <input type="text" class="form-control" v-model='buscador' placeholder="Buscar Blueray..."
                            aria-desc ribedby="basic-addon1">
                    </div>
                    <div class="col-4">
                        <button class="btn btn-success" data-toggle="modal" data-target="#nuevo" @click="reiniciarModel">Registrar
                            Blueray</button>
                    </div>
                </div>

                <table class="table table-striped col-12">
                    <thead>

                        <th>Título</th>
                        <th>Clasificación</th>
                        <th>País</th>
                        <th>Año</th>
                        <th>Director</th>
                        <th>Reparto</th>
                        <th>Calificación</th>
                        <th>Link</th>
                        <th>Estado</th>
                        <th>Acciones</th>
                    </thead>
                    <tbody>
                        <tr v-for="cd in filtro">

                            <td>{{cd.titulo}}</td>
                            <td>{{cd.clasificacion}}</td>
                            <td>{{cd.pais}}</td>
                            <td>{{cd.anio}}</td>
                            <td>{{cd.director}}</td>
                            <td>{{cd.reparto}}</td>
                            <td>{{cd.calificacion}}</td>
                            <td>{{cd.link}}</td>
                            <td>{{getEstado(cd.disponible)}}</td>
                            <td>
                                <button class="btn btn-round btn-primary" data-toggle="modal" data-target="#editar"
                                    @click="guardarEnModel(cd)"><span class="fa fa-pencil-alt"></span></button>
                                <button class="btn btn-round btn-danger" data-toggle="modal" data-target="#eliminar"
                                    @click="guardarEnModel(cd)"><span class="fa fa-trash"></span></button>
                            </td>

                        </tr>
                    </tbody>
                </table>


            </div>
        </div>
    </div>


    <!-- Modal agregar blueray -->
    <div class="modal fade" id="nuevo" tabindex="-1" role="dialog" aria-labelledby="exampleModalLongTitle" aria-hidden="true">
        <div class="modal-dialog" role="document">
            <div class="modal-content">
                <div class="modal-header">
                    <h5 class="modal-title" id="exampleModalLongTitle">Registrar Blueray</h5>
                    <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                        <span aria-hidden="true">&times;</span>
                    </button>
                </div>
                <form>
                <div class="modal-body">
                    <label for="">Titulo</label>
                    <input type="text" class="form-control" placeholder="Titulo" v-model="blueray.titulo" required>
                    <br>
                    <label for="">Clasificación</label>
                    <input type="text" class="form-control" placeholder="Clasificación" v-model="blueray.clasificacion" required>
                    <br>
                    <label for="">País</label>
                    <input type="text" class="form-control" placeholder="País" v-model="blueray.pais" required>
                    <br>
                    <label for="">Año</label>
                    <input type="number" class="form-control" placeholder="Año" v-model="blueray.anio" required>
                    <br>
                    <label for="">Director</label>
                    <input type="text" class="form-control" placeholder="Director" v-model="blueray.director" required>
                    <br>
                    <label for="">Reparto</label>
                    <input type="text" class="form-control" placeholder="Reparto" v-model="blueray.reparto" required>
                    <br>
                    <label for="">Calificación</label>
                    <input type="number" class="form-control" placeholder="Calificación" v-model="blueray.calificacion" required>
                    <br>
                    <label for="">Link</label>
                    <input type="text" class="form-control" placeholder="Link" v-model="blueray.link" required>
                    <div class="alert alert-danger mt-4" v-if="errorsForm.length">
                        <ul>
                            <li v-for="e in errorsForm">{{e}}</li>
                        </ul>
                    </div>
               
                </div>
                <div class="modal-footer">
                    <button type="button" class="btn btn-secondary" data-dismiss="modal">Cancelar</button>
                    <button type="button" @click="validar()" class="btn btn-primary" >Registrar
                        Blueray</button>
                </div>
                </form>
            </div>
        </div>
    </div>

    <!-- Modal editar blueray -->
    <div class="modal fade" id="editar" tabindex="-1" role="dialog" aria-labelledby="exampleModalLongTitle" aria-hidden="true">
        <div class="modal-dialog" role="document">
            <div class="modal-content">
                <div class="modal-header">
                    <h5 class="modal-title" id="exampleModalLongTitle">Editar Blueray</h5>
                    <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                        <span aria-hidden="true">&times;</span>
                    </button>
                </div>
                <form>
                <div class="modal-body">

                        <label for="">Titulo</label>
                        <input type="text" class="form-control" placeholder="Titulo" v-model="blueray.titulo" required>
                        <br>
                        <label for="">Clasificación</label>
                        <input type="text" class="form-control" placeholder="Clasificación" v-model="blueray.clasificacion" required>
                        <br>
                        <label for="">País</label>
                        <input type="text" class="form-control" placeholder="País" v-model="blueray.pais" required>
                        <br>
                        <label for="">Año</label>
                        <input type="number" class="form-control" placeholder="Año" v-model="blueray.anio" required>
                        <br>
                        <label for="">Director</label>
                        <input type="text" class="form-control" placeholder="Director" v-model="blueray.director" required>
                        <br>
                        <label for="">Reparto</label>
                        <input type="text" class="form-control" placeholder="Reparto" v-model="blueray.reparto" required>
                        <br>
                        <label for="">Calificación</label>
                        <input type="number" class="form-control" placeholder="Calificación" v-model="blueray.calificacion" required>
                        <br>
                        <label for="">Link</label>
                        <input type="text" class="form-control" placeholder="Link" v-model="blueray.link" required>
                        <div class="alert alert-danger mt-4" v-if="errorsForm.length">
                            <ul>
                                 <li v-for="e in errorsForm">{{e}}</li>
                            </ul>
                         </div>                           
                </div> 
                <div class="modal-footer">
                    <button type="button" class="btn btn-secondary" data-dismiss="modal">Cancelar</button>
                    <button type="button" @click="validarEdit()" class="btn btn-primary">Guardar Cambios</button>
                </div>
                </form>
            </div>
        </div>
    </div>

    <!-- Modal confirmar eliminacion -->
    <div class="modal sm" id="eliminar" tabindex="-1" role="dialog" aria-labelledby="eliminarUsuario" aria-hidden="true">
        <div class="modal-dialog" role="document">
            <div class="modal-content">
                <div class="modal-header">
                    <h5 class="modal-title" id="exampleModalLongTitle">Confirmar Eliminación</h5>
                    <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                        <span aria-hidden="true">&times;</span>
                    </button>
                </div>
                <div class="modal-body">
                    <center>¿Está seguro que desea eliminar <b>{{blueray.titulo}}</b>? </center>
                </div>
                <div class="modal-footer">
                    <button type="button" class="btn btn-secondary" data-dismiss="modal">Cancelar</button>
                    <button type="button" class="btn btn-danger" data-dismiss="modal" @click="eliminar(blueray.id_clave)">Eliminar</button>
                </div>
            </div>
        </div>
    </div>


</div>

<script>
    new Vue({
        el: '#app',
        data: {
            bluerays: [],
            buscador: '',
            blueray: {
                id_clave: "",
                titulo: "",
                clasificacion: "",
                pais: "",
                anio: "",
                director: "",
                reparto: "",
                califiacion: "",
                link: "",
                disponible: ""
            },
            errorsForm: []
        },
        mounted: function () {
            this.getData();
        },
        methods: {

            getEstado(estado){
                var estado = parseInt(estado);
                if(estado == 1) return "Disponible";
                else return "Prestado";
            },

            guardarEnModel(item) {
                this.errorsForm = [];
                this.blueray = Object.assign({}, item);
            },
            eliminar(id) {
                var url = "<?=base_url()?>index.php/api/blueray/" + id;

                axios.delete(url)
                    .then(response => {
                        // alert('Examen borrado');
                        this.getData();
                        this.reiniciarModel();

                    }).catch(error => {
                        console.log(error);
                        this.getData();
                    });

            },
            getData() {
                var url = "<?=base_url()?>index.php/api/blueray";
                axios.get(url).then(
                    response => {
                       this.bluerays = response.data;
                      
                        this.reiniciarModel();
                        
                      
                    }
                ).catch(
                    error => {
                        //alert('Error');
                        this.bluerays = [];
                    }
                );
            },
            registar() {
   
                var url = "<?=base_url()?>index.php/api/blueray";
                var data = {
                    titulo: this.blueray.titulo,
                    clasificacion: this.blueray.clasificacion,
                    pais: this.blueray.pais,
                    anio: this.blueray.anio,
                    director: this.blueray.director,
                    reparto: this.blueray.reparto,
                    calificacion: this.blueray.calificacion,
                    link: this.blueray.link

                };
                axios.post(url, data).then(
                    response => {
                        this.getData();
                        this.reiniciarModel();
                    }
                ).catch(
                    error => {
                        alert('Error');

                    }
                );
            },
            guardarCambios() {
                var url = "<?=base_url()?>index.php/api/blueray/" + this.blueray.id_clave;
                var data = {
                    titulo: this.blueray.titulo,
                    clasificacion: this.blueray.clasificacion,
                    pais: this.blueray.pais,
                    anio: this.blueray.anio,
                    director: this.blueray.director,
                    reparto: this.blueray.reparto,
                    calificacion: this.blueray.calificacion,
                    link: this.blueray.link
                };
                axios.put(url, data).then(
                    response => {
                        this.getData();
                        this.reiniciarModel();
                    }
                ).catch(
                    error => {
                        console.log(error);
                        alert('Error');
                    }
                );
            },
            reiniciarModel() {
                this.blueray = {
                    id_clave: "",
                    titulo: "",
                    clasificacion: "",
                    pais: "",
                    anio: "",
                    director: "",
                    reparto: "",
                    califiacion: "",
                    link: ""
                };
            },

            validar(){
                
                ok = true;
                this.errorsForm = [];

                if(!this.blueray.titulo){
                    this.errorsForm.push("Título requerido");
                    ok = false;
                }
                if(!this.blueray.clasificacion){
                    this.errorsForm.push("Clasificación requerido");
                    ok = false;
                }
                if(this.blueray.calificacion>10){
                    this.errorsForm.push("La calificación máxima es 10");
                    ok = false;
                }

                if(ok){
                    this.registar();
                    $('#nuevo').modal("hide");
                }
            },

            validarEdit(){
                
                ok = true;
                this.errorsForm = [];

                if(!this.blueray.titulo){
                    this.errorsForm.push("Título requerido");
                    ok = false;
                }
                if(!this.blueray.clasificacion){
                    this.errorsForm.push("Clasificación requerido");
                    ok = false;
                }
                if(this.blueray.calificacion>10){
                    this.errorsForm.push("La calificación máxima es 10");
                    ok = false;
                }


                if(ok){
                    this.guardarCambios();
                    $('#editar').modal('hide');
                }
            }
        },
        computed: {
            filtro: function () {
                if (this.buscador === '') {
                    return this.bluerays;
                } else {
                    return this.bluerays.filter((item) => {
                        return (item.titulo.toString()  + item.clasificacion
                            .toString()).toLowerCase().includes(this.buscador
                            .toLowerCase());
                    });
                }
            }

        }



    });

    /*  $(document).ready(function(){
         $('#tabla-usuarios').DataTable();
     });
       */
</script>