<div class="container-fluid" id="app">

    <div class="row" style="margin-top: 0%">
        <div class="col-sm-12" style="margin-top: 3%; background-color: #FFF0D1">
            <div class="col-sm-12">
                <h4>Número de cuenta del usuario:</h4>
            </div>

            <div class="col-sm-4">
                <input type="number" v-model="cuentaUsuario" class="form-control">
                <br>
                <button class="btn btn-success" @click="buscarUsuario(),getPrestamos()">Buscar</button>
            </div>
            <div class="col-sm-8">
                <div v-if="usuario.nombre">
                    <b>Nombre del usuario: </b> {{usuario.nombre}} {{usuario.ape_pat}} {{usuario.ape_mat}} <br>
                    <b>Correo: </b> {{usuario.correo}}<br>
                    <b>Adeudo: </b> ${{usuario.adeudo}}<br>
                </div>
            </div>
        </div>

        <div class="col col-sm-12 col-md-6">
            <div class="col-sm-12">
                <h3>Seleccionar material</h3>
            </div>

            <div class="col-sm-12">
                <select class="col-sm-10" @change="buscador = '';getMaterial()" v-model="tipoMaterial">
                    <option value="" disabled selected>Selecciona tipo de material</option>
                    <option value="1">Música</option>
                    <option value="2">Libros</option>
                    <option value="3">Películas</option>
                    <option value="4">BlueRay</option>
                </select>
            </div>
            <div class="col-sm-12" style="margin-top: 3%">
                <div class="col-sm-8">
                    <input type="text" class="form-control" v-model='buscador' placeholder="Buscar..." aria-desc
                        ribedby="basic-addon1">
                </div>
                <div class="col-sm-4">
                    <button class="btn btn-success" @click="getMaterial">Buscar</button>
                </div>
            </div>
            <div v-if="tipoMaterial">
                <nav aria-label="Page navigation example" class="col-sm-12">
                    <center>
                        <ul class="pagination">
                            <li class="page-item" v-if="pagina != 0"><a class="page-link"
                                    @click="pagina-= 1;getMaterial()" href="#">Anterior</a></li>
                            <li class="page-item" v-for="(p,index) in totalPaginas"><a
                                    @click="pagina = index; getMaterial()" class="page-link" href="#">{{p}}</a></li>

                            <li class="page-item" v-if="pagina != totalPaginas-1"> <a class="page-link"
                                    @click="pagina+= 1;getMaterial()" href="#">Siguiente</a></li>
                        </ul>
                    </center>
                </nav>
                <table class="table col-sm-12" v-if="tipoMaterial === '1'">
                    <thead>
                        <th>#</th>
                        <th>Título</th>
                        <th>Interprete</th>
                        <th>Clasificación</th>
                        <th>
                        </th>
                    </thead>
                    <tbody>
                        <tr v-for="(m,index) in materialDisponible">
                            <td>{{(pagina*cantidad)+index+1}}</td>
                            <td>{{m.titulo}}</td>
                            <td>{{m.interprete}}</td>
                            <td>{{m.clasificacion}}</td>
                            <td> <button class="btn btn-primary" @click="agregarMaterial(m,1)">
                                    <i class="fa fa-plus"></i>
                                </button></td>
                        </tr>
                    </tbody>
                </table>
                <table class="table " v-if="tipoMaterial === '2'">
                    <thead>
                        <th>Título</th>
                        <th>Autor</th>
                        <th>Clasificación</th>
                        <th>
                        </th>
                    </thead>
                    <tbody>
                        <tr v-for="m in materialDisponible">
                            <td>{{m.titulo}}</td>
                            <td>{{m.libro.autor}}</td>
                            <td>{{m.clasificacion}}</td>
                            <td> <button class="btn btn-primary" @click="agregarMaterial(m,2)">
                                    <i class="fa fa-plus"></i>
                                </button></td>
                        </tr>
                    </tbody>
                </table>
                <table class="table " v-if="tipoMaterial === '3'">
                    <thead>
                        <th>Título</th>
                        <th>Director</th>
                        <th>Clasificación</th>
                        <th>
                        </th>
                    </thead>
                    <tbody>
                        <tr v-for="m in materialDisponible">
                            <td>{{m.titulo}}</td>
                            <td>{{m.pelicula.director}}</td>
                            <td>{{m.clasificacion}}</td>
                            <td> <button class="btn btn-primary" @click="agregarMaterial(m,3)">
                                    <i class="fa fa-plus"></i>
                                </button></td>
                        </tr>
                    </tbody>
                </table>
                <table class="table " v-if="tipoMaterial === '4'">
                    <thead>
                        <th>Título</th>
                        <th>Director</th>
                        <th>Clasificación</th>
                        <th>
                        </th>
                    </thead>
                    <tbody>
                        <tr v-for="m in materialDisponible">
                            <td>{{m.titulo}}</td>
                            <td>{{m.blueray.director}}</td>
                            <td>{{m.clasificacion}}</td>
                            <td> <button class="btn btn-primary" @click="agregarMaterial(m,4)">
                                    <i class="fa fa-plus"></i>
                                </button></td>
                        </tr>
                    </tbody>
                </table>
                <nav aria-label="Page navigation example" class="col-sm-12">
                    <center>
                        <ul class="pagination">
                            <li class="page-item" v-if="pagina != 0"><a class="page-link"
                                    @click="pagina-= 1;getMaterial()" href="#">Anterior</a></li>
                            <li class="page-item" v-for="(p,index) in totalPaginas"><a
                                    @click="pagina = index; getMaterial()" class="page-link" href="#">{{p}}</a></li>

                            <li class="page-item" v-if="pagina != totalPaginas-1"> <a class="page-link"
                                    @click="pagina+= 1;getMaterial()" href="#">Siguiente</a></li>
                        </ul>
                    </center>
                </nav>
            </div>
        </div>
        <div class="col-sm-12 col-md-6">
            <h3>Hacer préstamo</h3>
            <table class="table">
                <thead>
                    <th>Título</th>
                    <th>Tipo</th>
                    <th>Quitar</th>
                </thead>
                <tbody>
                    <tr v-for="(m,index) in materialAPrestar">
                        <td>{{m.titulo}} - <small>{{m.autor}}</td>
                        <td>{{m.tipo}} </small></td>
                        <td><button class="btn btn-danger" @click="quitarMaterial(index)">
                                <i class="fa fa-minus"></i>
                            </button></td>

                    </tr>
                </tbody>
            </table>
            <div class="col-sm-12" style="width: 70%; margin-left: 0%">
                <label>Fecha de devolución:</label>
                <input type="date" v-model="fechaDevolucion" class="form-control">
                <br>
            </div>
            <div class="col-sm-12">

                <center> <button class="btn btn-primary" @click="validar()" @click="guardarPrestamos" >Registrar préstamo</button>

                

                </center>
            </div>
        </div>


        <div class="col-sm-12" style="margin-top: 3%; background-color: #FCFBA2">
            <div class="card-body" >

                <div class="row" style="margin-bottom: 15px;">
                <h3>Historial de préstamos del usuario: {{usuario.nombre}} {{usuario.ape_pat}}</h3>
                </div>

                <table class="table table-striped col-12">
                    <thead>
                        <th>Título</th>
                        <th>Clasificación</th>
                        <th>Prestador</th>

                        <th>Usuario</th>

                        <th>Fecha de préstamo</th>
                        <th>Fecha prometida de devolución</th>
                        <th>Tipo</th>
                    </thead>
                    <tbody>
                        <tr v-for="prestamo in prestamos" >
                            <td>{{prestamo.titulo}}</td>
                            <td>{{prestamo.clasificacion}}</td>
                            <td>{{prestamo.nombre_p}} {{prestamo.ape_pat_p}} {{prestamo.ape_mat_p}}</td>
                            <td>{{prestamo.nombre_u}} {{prestamo.ape_pat_u}} {{prestamo.ape_mat_u}}</td>

                            <td>{{prestamo.fecha_chida}}</td>
                            <td :style="validarFecha(prestamo.fecha_prometida)">{{prestamo.fecha_prometida}}</td>
                            <td>{{getTipo(prestamo.id_tipo)}}</td>

                        </tr>
                    </tbody>
                </table>


            </div>
        </div>
    </div>



<!-- Modal -->
<div class="modal fade" id="mensaje" tabindex="-1" role="dialog" aria-labelledby="exampleModalLabel" aria-hidden="true">
  <div class="modal-dialog" role="document">
    <div class="modal-content">
      <div class="modal-header">
        <h3 class="modal-title" id="exampleModalLabel">Aviso</h3> 
        <button type="button" class="close" data-dismiss="modal" aria-label="Close">
          <span aria-hidden="true">&times;</span>
        </button>
      </div>
      <div class="modal-body">
        {{mensaje}}
      </div>
      <div class="modal-footer">
        <button type="button" class="btn btn-primary" data-dismiss="modal">OK</button>
      </div>
    </div>
  </div>
</div>

</div>
<script>
    let app = new Vue({
        el: "#app",
        data: {
            prestamos: [],
            tipoMaterial: "",
            buscador: "",
            materialDisponible: [],
            modelPrestamo: {
                clasificacion: "",
                autor: "",
                titulo: "",
                imprenta: "",
                paginas: "",
                isbn: "",
                temas: ""
            },
            materialAPrestar: [

            ],
            cuentaUsuario: "",
            usuario: {
                id_usuario: "",
                nombre: "",
                ape_pat: "",
                ape_mat: "",
                correo: "",
                telefono: "",
                adeudo: ""
            },
            fechaDevolucion: "",
            pagina: 0,
            cantidad: 15,
            cantidadRegistros: 0,
            mensaje: ""
        },
        methods: {
            validar(){
                
                ok = true;
                this.errorsForm = [];

                if(!this.cuentaUsuario){
                    this.mensaje = "Número de cuenta de usuario requerido";
                    $("#mensaje").modal("show");
                    ok = false;
                }else{
                    if(this.materialAPrestar==0){
                    this.mensaje = "Seleccione material a prestar";
                    $("#mensaje").modal("show");
                    ok = false;
                }else{
                    if(!this.fechaDevolucion){
                    this.mensaje = "Seleccione fecha  de devolución";
                    $("#mensaje").modal("show");
                    ok = false;
                }

                }
                }
              
                if(ok){
                    this.guardarPrestamos();
                }
            },
            getPrestamos() {
                axios.get("<?=base_url()?>index.php/api/historialPrestamos/" + this.cuentaUsuario).then(
                    response => {
                        this.prestamos = response.data;
                    }
                )
            },
            getMaterial() {
                axios.get("<?=base_url()?>index.php/api/materialdisponible/" + this.tipoMaterial + "/" + this.pagina + "/" + this.cantidad + "/" + this.buscador).then(
                    response => {
                        this.materialDisponible = response.data.resultado;
                        this.cantidadRegistros = response.data.cantidad;

                    }
                )
            },
            getTipo(tipo) {
                if (tipo === '1') return "Musica";
                if (tipo === '2') return "Libro";
                if (tipo === '3') return "Película";
                if (tipo === '4') return "BlueRay";
            },
            agregarMaterial(material, tipo) {
                registro = {
                    id_clave: material.id_clave,
                    titulo: material.titulo,
                    tipoNumerico: tipo,
                    autor: "",
                    tipo: "",
                    pagina: 0,
                    cantidad: 10
                };

                switch (tipo) {
                    case 1:
                        registro.tipo = "Música";
                        registro.autor = material.interprete;
                        break;
                    case 2:
                        registro.tipo = "Libro"
                        registro.autor = material.libro.autor;
                        break;
                    case 3:
                        registro.tipo = "Pelicula"
                        registro.autor = material.pelicula.director;
                        break;
                    case 4:
                        registro.tipo = "Blueray"
                        registro.autor = material.blueray.director;
                        break;

                    default:
                        break;
                }
                this.materialAPrestar.push(registro);
            },
            buscarUsuario() {
                axios.get("api/datosUsuario/" + this.cuentaUsuario)
                    .then(response => {
                        this.usuario = response.data;
                    }).catch(
                        error => {
                            alert("Usuario no encontrado");
                        }
                    )
            },
            validarFecha(fecha){
                var fechaActual = new Date();
                fechaPartida = fecha.split("-");
                anio = fechaPartida[0];
                mes = parseInt(fechaPartida[1])-1;
                dia = fechaPartida[2];

                fechaPrometida = new Date(anio,mes,dia);
         
                if(fechaActual > fechaPrometida){
                    return "background-color: #C2E6FF";
                }
                else return "background-color: yellow";
            },
            guardarPrestamos() {

                prestamos = [];

                var today = new Date();
                var dd = String(today.getDate()).padStart(2, '0');
                var mm = String(today.getMonth() + 1).padStart(2, '0'); //January is 0!
                var yyyy = today.getFullYear();
                fecha = yyyy + "-" + mm + "-" + dd;
                for (material of this.materialAPrestar) {
                    prestamos.push({
                        id_clave: material.id_clave,
                        id_prestador: "<?=$this->session->userdata('id_prestador'); ?>",
                        id_usuario: this.cuentaUsuario,
                        fecha_prestamo: yyyy + "-" + mm + "-" + dd,
                        fecha_prometida: this.fechaDevolucion
                    });
                }

                axios.post("api/guardarPrestamos", {
                    prestamos: prestamos
                })
                    .then(response => {
                        this.mensaje = "Registro exitoso";
                        $('#mensaje').modal('show');
                        this.materialAPrestar = [];
                        this.cuentaUsuario = "";
                        this.usuario.nombre = "";
                        this.usuario.ape_pat = "";
                        this.usuario.ape_mat = "";
                        this.usuario.correo = "";
                        this.usuario.adeudo = "";
                        this.buscador = "";
                        this.getMaterial();

                    }).catch(
                        error => {
                            this.mensaje = "Ocurrió un error al guardar";
                            $('#mensaje').modal('show');
                           
                        }
                    )
            },
            quitarMaterial(index) {
                this.materialAPrestar.splice(index, 1);
            }
        },
        created: function () {

        },
        computed: {
            totalPaginas() {
                return Math.ceil(this.cantidadRegistros / this.cantidad);

            }
        }
    })
</script>