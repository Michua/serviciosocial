<div id="app" class="row" style="margin-top: 25px;">
    <div class="col-12">
        <div class="card">     
            <div class="card-header bg-light">
                <h1>Devolución</h1>
            </div>
            <div class="card-body">

                <div class="row" style="margin-bottom: 15px;">
                    <div class="col-sm-12">
                        <div class="col-sm-8">
                            <div class="col-sm-12">
                                <h4>Número de cuenta del usuario</h4>
                                <div class="col-sm-4">
                                    <input type="number" v-model="cuentaUsuario" class="form-control">
                                    <br>
                                    <button class="btn btn-success" @click="buscarPrestamos()">Buscar</button>
                                </div>
                                <div class="col-sm-8">
                                    <div v-if="prestamos.usuario">
                                        <b>Nombre del usuario: </b> {{prestamos.usuario.nombre}}
                                        {{prestamos.usuario.ape_pat}} {{prestamos.usuario.ape_mat}}
                                        <br>
                                        <b>Correo: </b> {{prestamos.usuario.correo}}<br>
                                        <b>Adeudo: </b> ${{prestamos.usuario.adeudo}}<br>
                                    </div>
                                </div>
                            </div>
                        </div>
                        <div class="col-sm-4">
                            <br><br><br><br>
                            <button class="btn btn-round btn-primary" 
                                @click="devolverPrestamo()">Devolver todo</button>
                            <button class="btn btn-round btn-warning" data-toggle="modal" data-target="#renovar"
                                @click="renovarPrestamosTodos()">Renovar todo</button>
                        </div>

                    </div>
                </div>

                <table class="table table-striped col-12">
                    <thead>
                        <th>Título</th>
                        <th>Clasificación</th>
                        <th>Prestador</th>
                        <th>Usuario</th>
                        <th>Fecha de préstamo</th>
                        <th>Fecha prometida de devolución</th>
                        <th></th>
                        <th></th>
                    </thead>
                    <tbody>
                        <tr v-for="p in prestamos.prestamos">

                            <td>{{p.titulo}}</td>
                            <td>{{p.clasificacion}}</td>
                            <td>{{p.nombre_p}} {{p.ape_pat_p}} {{p.ape_mat_p}}</td>
                            <td>{{p.nombre_u}} {{p.ape_pat_u}} {{p.ape_mat_u}}</td>
                            <td>{{p.fecha_chida}}</td>
                            <td>{{p.fecha_prometida}}</td>
                            <td>
                                <button class="btn btn-round btn-primary" data-toggle="modal"
                                    data-target="#devolver" @click="devolverPrestamoIndividual(p.id_prestamo)">Devolver</button>

                            </td>
                            <td> <button class="btn btn-round btn-warning" data-toggle="modal"
                                    data-target="#renovar" @click="renovarPrestamo(p.id_prestamo)">Renovar</button>
                            </td>
                        </tr>

                    </tbody>
                </table>


            </div>
        </div>
    </div>
    
</div> 

<script>
    new Vue({
        el: '#app',
        data: {
            tipoMaterial: "",
            prestamos: [],
            prestamoDisponible: [],
            cuentaUsuario: "",
            prestamosModel:{
                id_prestamo:"",
            },
            usuario: {
                id_usuario: "",
                nombre: "",
                ape_pat: "",
                ape_mat: "",
                correo: "",
                telefono: "",   
                adeudo: ""
            },
        },
        methods: {
            buscarPrestamos() {
                axios.get("api/prestamosUsuario/" + this.cuentaUsuario)
                    .then(response => {
                        this.prestamos = response.data;
                    }).catch(
                        error => {
                            alert("Usuario no encontrado");
                        }
                    )
            },
            renovarPrestamosTodos() {
                axios.put("api/renovarPrestamos/" + this.cuentaUsuario)
                    .then(response => {
                        this.prestamos = response.data;
                        this.buscarPrestamos();
                    }).catch(
                        error => {
                            alert("No se puede renovar los materiales");
                        }
                    )
            },
            renovarPrestamo(id_prestamo) {
                axios.put("api/renovarPrestamo/" + id_prestamo)
                    .then(response => {
                        this.prestamos = response.data;
                        this.buscarPrestamos();
                    }).catch(
                        error => {
                            alert("No se puede renovar el material");
                        }
                    )
            },
            devolverPrestamo() {
               
                axios.put("api/devolverPrestamo/",{
                    id_usuario:this.prestamos.usuario.id_usuario
                })
                    .then(response => {
                        this.prestamos = response.data;
                        this.buscarPrestamos();
                    }).catch(
                        error => {
                            alert("No se puede devolver el material");
                        }
                    )
            },
            devolverPrestamoIndividual(id_prestamo) {
               
               axios.put("api/devolverPrestamoIndividual/" + id_prestamo)
                   .then(response => {
                       this.prestamos = response.data;
                       this.buscarPrestamos();
                   }).catch(
                       error => {
                           alert("No se puede devolver el material");
                       }
                   )
           },

        },




    });



    /*  $(document).ready(function(){
         $('#tabla-usuarios').DataTable();
     });
       */
</script>