<div id="app" class="row" style="margin-top: 25px;">
    <div class="col-12">
        <div class="card">
            <div class="card-header bg-light">
                <h1>Prestamos</h1>
                <div class="col-sm-12" style="margin-top: 3%;">
                        <h4>Número de cuenta del usuario</h4>
                        <div class="col-sm-4">
                            <input type="number" v-model="cuentaUsuario" class="form-control">
                            <br>
                            <button class="btn btn-success" @click="buscarUsuario()">Buscar</button>
                        </div>
                        <div class="col-sm-8">
                            <div v-if="usuario.nombre">
                                <b>Nombre del usuario: </b> {{usuario.nombre}} {{usuario.ape_pat}} {{usuario.ape_mat}} <br>
                                <b>Correo: </b> {{usuario.correo}}<br>
                                <b>Adeudo: </b> ${{usuario.adeudo}}<br>
                            </div>
                        </div>
                    </div>
            </div>
            <div class="card-body">

                <table class="table table-striped col-12" v-if="prestamos.length">
                    <thead>
                        <th>Título</th>
                        <th>Clasificación</th>
                        <th>Prestador</th>

                        <th>Usuario</th>

                        <th>Fecha de préstamo</th>
                        <th>Tipo</th>
                         <th></th>
                    </thead>
                    <tbody>
                        <tr v-for="prestamo in prestamos">
                            <td>{{prestamo.titulo}}</td>
                            <td>{{prestamo.clasificacion}}</td>
                            <td>{{prestamo.nombre_p}} {{prestamo.ape_pat_p}} {{prestamo.ape_mat_p}}</td>
                            <td>{{prestamo.nombre_u}} {{prestamo.ape_pat_u}} {{prestamo.ape_mat_u}}</td>
                            <td>{{prestamo.fecha_prestamo}}</td>
                            <td>{{getTipo(prestamo.id_tipo)}}</td>
                            <td> <button class="btn btn-success" @click="devolverMaterial(prestamo.id_prestamo,prestamo.id_clave)">Devolver</button></td>
                        </tr>
                    </tbody>
                </table>


            </div>
        </div>
    </div>
</div>

<script>
    new Vue({
        el: '#app',
        data: {
            tipoMaterial: "",
            prestamos: [],
            prestamoDisponible: [],
            cuentaUsuario: "",
            usuario: {}

        },
        mounted: function () {

        },
        methods: {
            getTipo(tipo) {
                if (tipo === '1') return "Musica";
                if (tipo === '2') return "Libro";
                if (tipo === '3') return "Película";
                if (tipo === '4') return "BlueRay";
            },
            buscarUsuario() {
                    axios.get("api/datosUsuario/" + this.cuentaUsuario)
                        .then(response => {
                            this.usuario = response.data;
                        }).catch(
                            error => {
                                alert("Usuario no encontrado");
                            }
                        );

                        axios.get("api/prestamosUsuario/" + this.cuentaUsuario)
                        .then(response => {
                            this.prestamos = response.data;
                        }).catch(
                            error => {
                                alert("Usuario no encontrado");
                            }
                        )
                },
                devolverMaterial(id_prestamo,id_clave){
                    var today = new Date();
                    var dd = String(today.getDate()).padStart(2, '0');
                    var mm = String(today.getMonth() + 1).padStart(2, '0'); 
                    var yyyy = today.getFullYear();
                    fecha = yyyy + "-" + mm + "-" + dd;

                    axios.put("api/prestamosUsuario",{
                        id_prestamo: id_prestamo,
                        id_clave: id_clave,
                        fecha_devolucion: fecha
                    }).then(response => {
                            alert("Exito");
                            this.buscarUsuario();
                        }).catch(
                            error => {
                                alert("Error");
                            }
                        )
                },
        },




    });

    /*  $(document).ready(function(){
         $('#tabla-usuarios').DataTable();
     });
       */
</script>